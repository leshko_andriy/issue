<?php

namespace Oro\Bundle\IssueBundle\Tests\Behat;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Nelmio\Alice\Instances\Collection as AliceCollection;
use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\TestFrameworkBundle\Behat\Isolation\ReferenceRepositoryInitializerInterface;

/**
 * set reference data for issue fixture
 */
class ReferenceRepositoryInitializer implements ReferenceRepositoryInitializerInterface
{
    public const ISSUE_PRIORITY_MAJOR = 'issue_priority_major';
    public const ISSUE_STATUS_OPEN = 'issue_status_open';

    /**
     * {@inheritdoc}
     */
    public function init(Registry $doctrine, AliceCollection $referenceRepository): void
    {
        $this->setEnumReferences($doctrine, $referenceRepository);
    }

    /**
     * @param Registry $doctrine
     * @param AliceCollection $referenceRepository
     */
    private function setEnumReferences(Registry $doctrine, AliceCollection $referenceRepository): void
    {
        $manager = $doctrine->getManager();
        $classPriorityName = ExtendHelper::buildEnumValueClassName(
            IssuePriorityProvider::ENUM_CODE_PRIORITY
        );
        $classStatusName = ExtendHelper::buildEnumValueClassName(
            IssueStatusProvider::ENUM_CODE_STATUS
        );

        $priorityMajor = $manager->getRepository($classPriorityName)
            ->find(IssuePriorityProvider::PRIORITY_MAJOR);
        $statusOpen = $manager->getRepository($classStatusName)
            ->find(IssueStatusProvider::STATUS_OPEN);


        $referenceRepository->set(self::ISSUE_PRIORITY_MAJOR, $priorityMajor);
        $referenceRepository->set(self::ISSUE_STATUS_OPEN, $statusOpen);
    }
}

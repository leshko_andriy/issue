Feature: Issue CRUD operations
    In order to manage Issues
    As Administrator
    I need to be able to view, create, edit and delete Issues

  Scenario: Create new Issue
      Given I login as administrator
      And I go to Issues/Issues
      And press "Create Issue"
      And fill form with:
          | Summary     | Summary 123456       |
          | Description | Summary description  |
          | Priority    | Critical             |
          | Type        | Bug                  |
          | Assignee    | John Doe             |
      When I press "Save and Close"
      Then I should see "Issue saves" flash message

  Scenario: View Issue in issue index page
      Given I go to Issues/Issues
      When I click view "Summary 123456" in "Issue Grid"
      And I should see issue with:
          | Summary     | Summary 123456       |
          | Description | Summary description  |
          | Priority    | Critical             |
          | Type        | Bug                  |
          | Assignee    | John Doe             |
      And John Doe should be an owner

  Scenario: Edit Issue
      Given I go to Issues/Issues
      When I click update "Summary 123456" in "Issue Grid"
      And fill form with:
          | Summary     | Summary 123456 (EDIT)       |
          | Description | Summary description (EDIT)  |
          | Priority    | Major                       |
          | Type        | Story                       |
          | Assignee    | John Doe                    |
      And press "Save and Close"
      Then I should see "Issue saves" flash message

  Scenario: Create subtask Issue
      Given I go to Issues/Issues
      When I click view "Summary 123456 (EDIT)" in "Issue Grid"
      And press "Create subtask Issue"
      And fill form with:
          | Summary     | Summary 123456 Subtask |
          | Description | Summary description    |
          | Priority    | Critical               |
          | Assignee    | John Doe               |
      And I press "Save"
      Then I go to Issues/Issues
      When I click view "Summary 123456 Subtask" in "Issue Grid"
      And I should see issue with:
          | Summary     | Summary 123456 Subtask |
          | Description | Summary description    |
          | Priority    | Critical               |
          | Type        | SubTask                |

  Scenario: Delete Issue
      Given I go to Issues/Issues
      When I keep in mind number of records in list
      And I click Delete "Summary 123456 (EDIT)" in "Issue Grid"
      And confirm deletion
      Then I should see "Issue Deleted" flash message

  Scenario: See widget issue on dashboard
      Given I go to Dashboards/Dashboard
      And I should see "My last active Issues"
      And I should see "Issues statuses"

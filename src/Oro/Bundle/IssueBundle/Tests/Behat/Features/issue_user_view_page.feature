Feature: Issue user view page
    In order to manage Issues on user page
    As Administrator
    I need to be able to manage my issue in user page

  Scenario: See widget my issue
      Given I login as administrator
      And press "User menu"
      And press "My User"
      And I should see "Issues" in the "Issue Grid title" element

  Scenario: Create my issue from user view page
      Given press "More action"
      And press "Create Issue User view"
      And fill form with:
          | Summary     | Summary 123456 My Issue |
          | Description | Summary description     |
          | Priority    | Critical                |
          | Type        | Bug                     |
      And I press "Save"
      And I should see following "User Issues Grid" grid:
          | Summary 123456 My Issue | Bug | John Doe   | John Doe   | Open | | Critical |
      Then I go to Issues/Issues
      When I click view "Summary 123456 My Issue" in "Issue Grid"
      And I should see issue with:
          | Summary     | Summary 123456 My Issue |
          | Description | Summary description     |
          | Priority    | Critical                |
          | Type        | Bug                     |
          | Assignee    | John Doe                |
      And John Doe should be an owner

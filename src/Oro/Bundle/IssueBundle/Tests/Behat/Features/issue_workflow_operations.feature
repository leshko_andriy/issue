@fixture-OroIssueBundle:issues.yml
Feature: Issue workflow operations
    In order to manage Issues
    As Administrator
    I need to be able to change workflow for Issues

  Scenario: Start Issue
      Given I login as administrator
      And I go to Issues/Issues
      When I click view "Summary test workflow" in "Issue Grid"
      And press "Start issue"
      Then I should see issue with:
          | Status      | In progress     |

  Scenario: Stop progress
      Given press "Stop progress"
      Then I should see issue with:
          | Status      | Open          |

  Scenario: Start Issue
      Given press "Start issue"
      Then I should see issue with:
          | Status      | In progress   |

  Scenario: Resolve Issue
      Given press "Resolve issue"
      And fill form with:
          | Resolution | Done           |
      When I press "Submit"
      Then I should see issue with:
          | Status      | Resolved      |
          | Resolution  | Done          |

  Scenario: Reopen Issue
      Given press "Reopen issue"
      Then I should see issue with:
          | Status      | Reopen        |
          | Resolution  | N/A           |

  Scenario: Start Issue
      Given press "Start issue"
      Then I should see issue with:
          | Status      | In progress   |

  Scenario: Close Issue
      Given press "Close issue"
      Then I should see issue with:
          | Status      | Close         |

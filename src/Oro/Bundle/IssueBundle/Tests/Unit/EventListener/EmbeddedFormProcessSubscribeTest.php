<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\EventListener;

use Oro\Bundle\EmbeddedFormBundle\Entity\EmbeddedForm;
use Oro\Bundle\EmbeddedFormBundle\Event\EmbeddedFormSubmitAfterEvent;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\EventListener\EmbeddedFormProcessSubscribe;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormInterface;

/**
 * Test embedded form subscribe. Auto set organization
 */
class EmbeddedFormProcessSubscribeTest extends TestCase
{
    public function testSetOrganization(): void
    {
        /** @var FormInterface|MockObject $form */
        $form = $this->createMock(FormInterface::class);

        $issue = new Issue();
        $org = new Organization();
        $entityEmbedded = new EmbeddedForm();
        $entityEmbedded->setOwner($org);

        $subscriber = new EmbeddedFormProcessSubscribe();
        $event = new EmbeddedFormSubmitAfterEvent($issue, $entityEmbedded, $form);
        $subscriber->setOrganization($event);

        self::assertEquals($issue->getOrganization(), $org);
    }
}

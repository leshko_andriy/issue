<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository;
use Oro\Bundle\IssueBundle\EventListener\IssueCodeEventListener;
use Oro\Component\Testing\Unit\EntityTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Unit test doctrine listener autogenerate code
 */
class IssueCodeEventListenerTest extends TestCase
{
    use EntityTrait;

    /**
     * @var EntityManager|MockObject
     */
    protected $em;

    /**
     * @var UnitOfWork|MockObject
     */
    protected $uow;

    /**
     * @var IssueRepository|MockObject
     */
    protected $repo;

    protected function setUp(): void
    {
        $this->em  = $this->createMock(EntityManager::class);

        $this->repo = $this->createMock(IssueRepository::class);

        $this->uow = $this->createMock(UnitOfWork::class);
    }

    public function testPostPersist(): void
    {
        $tmpIssue = new Issue();
        $tmpIssue->setCode('T-2');

        $this->repo
            ->expects($this->once())
            ->method('findOneBy')
            ->will($this->returnValue(
                false
            ));

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->will($this->returnValue($this->repo));

        $issue = $this->getEntity(Issue::class, [
            'id' => 1,
            'summary' => 'test'
        ]);

        $this->uow
            ->expects($this->once())
            ->method('scheduleExtraUpdate')
            ->with(
                $issue,
                ['code' => [null, 'T-1']]
            );

        $this->em
            ->expects($this->once())
            ->method('getUnitOfWork')
            ->will($this->returnValue($this->uow));

        $listener = new IssueCodeEventListener();

        $args = new LifecycleEventArgs($issue, $this->em);

        $listener->postPersist($issue, $args);
    }

    public function testUniquePostPersist(): void
    {
        $tmpIssue = new Issue();
        $tmpIssue->setCode('T-2');

        $this->repo
            ->expects($this->once())
            ->method('findOneBy')
            ->will($this->returnValue(
                true
            ));

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->will($this->returnValue($this->repo));

        $issue = $this->getEntity(Issue::class, [
            'id' => 2,
            'summary' => 'test'
        ]);

        $this->uow
            ->expects($this->once())
            ->method('scheduleExtraUpdate')
            ->with(
                $issue,
                $this->callback(function ($changes) {
                    return strstr($changes['code'][1], 'T-2') && $changes['code'][1] != 'T-2';
                })
            );

        $this->em
            ->expects($this->once())
            ->method('getUnitOfWork')
            ->will($this->returnValue($this->uow));

        $listener = new IssueCodeEventListener();

        $args = new LifecycleEventArgs($issue, $this->em);

        $listener->postPersist($issue, $args);
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\EventListener;

use Oro\Bundle\EntityBundle\Tools\EntityRoutingHelper;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Oro\Bundle\FormBundle\Form\Type\Select2EntityType;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\EventListener\FormSetAssigneeEventListener;
use Oro\Bundle\UserBundle\Entity\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\ResolvedFormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Test listener for form, Auto add assignee and block this field
 */
class FormSetAssigneeEventListenerTest extends TestCase
{
    /**
     * @var EntityRoutingHelper|MockObject
     */
    private $entityRoutingHelper;

    /**
     * @var RequestStack|MockObject
     */
    private $requestStack;

    /**
     * @var FormSetAssigneeEventListener
     */
    private $listener;

    protected function setUp(): void
    {
        $this->requestStack = $this->createMock(RequestStack::class);
        $this->entityRoutingHelper = $this->createMock(EntityRoutingHelper::class);

        $this->listener = new FormSetAssigneeEventListener(
            $this->entityRoutingHelper,
            $this->requestStack
        );
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testSetOwnerAndLockFormWithNotApplicableAction(): void
    {
        $currentRequest = $this->createMock(Request::class);

        $this->requestStack
            ->expects($this->once())
            ->method('getCurrentRequest')
            ->willReturn($currentRequest);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getAction')
            ->with($currentRequest)
            ->willReturn(FormSetAssigneeEventListener::ACTION_CREATE_ISSUE_MY);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getEntityClassName')
            ->with($currentRequest)
            ->willReturn(User::class);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getEntityId')
            ->with($currentRequest)
            ->willReturn(1);

        $this->entityRoutingHelper->expects($this->once())
            ->method('getEntity')
            ->with(User::class, 1)
            ->willReturn(new User());

        $form = $this->createMock(FormInterface::class);
        $assigneeForm = $this->createMock(FormInterface::class);

        $assigneeFormConfig = $this->createMock(FormConfigInterface::class);
        $assigneeFormConfigType = $this->createMock(ResolvedFormTypeInterface::class);
        $formConfigInnerType = $this->createMock(Select2EntityType::class);

        $assigneeFormConfigType->expects($this->once())
            ->method('getInnerType')
            ->willReturn($formConfigInnerType);

        $assigneeFormConfig->expects($this->any())
            ->method('getOptions')
            ->willReturn([
                'attr' => ['readonly' => false]
            ]);

        $assigneeFormConfig->expects($this->once())
            ->method('getType')
            ->willReturn($assigneeFormConfigType);

        $assigneeForm->expects($this->once())
            ->method('getConfig')
            ->willReturn($assigneeFormConfig);

        $form->expects($this->once())
            ->method('get')
            ->with('assignee')
            ->willReturn($assigneeForm);

        $form->expects($this->once())
            ->method('add')
            ->with('assignee', get_class($formConfigInnerType), ['attr' => ['readonly' => true]]);

        $issue = new Issue();
        $event = $this->createMock(FormProcessEvent::class);
        $event->expects($this->once())
            ->method('getData')
            ->willReturn($issue);

        $event->expects($this->once())
            ->method('getForm')
            ->willReturn($form);

        $this->listener->setAssigneeAndLockForm($event);

        self::assertInstanceOf(User::class, $issue->getAssignee());
    }
}

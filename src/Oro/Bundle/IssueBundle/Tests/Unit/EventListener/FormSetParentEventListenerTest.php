<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\EventListener;

use Oro\Bundle\EntityBundle\Tools\EntityRoutingHelper;
use Oro\Bundle\EntityConfigBundle\Form\Type\ChoiceType;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Oro\Bundle\FormBundle\Form\Type\Select2EntityType;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\EventListener\FormSetParentEventListener;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\ResolvedFormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Test listener for form, Auto add parent issue and block this field
 */
class FormSetParentEventListenerTest extends TestCase
{
    /**
     * @var EntityRoutingHelper|MockObject
     */
    private $entityRoutingHelper;

    /**
     * @var RequestStack|MockObject
     */
    private $requestStack;

    /**
     * @var FormSetParentEventListener
     */
    private $listener;

    protected function setUp(): void
    {
        $this->requestStack = $this->createMock(RequestStack::class);
        $this->entityRoutingHelper = $this->createMock(EntityRoutingHelper::class);

        $this->listener = new FormSetParentEventListener(
            $this->entityRoutingHelper,
            $this->requestStack
        );
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testSetOwnerAndLockFormWithNotApplicableAction(): void
    {
        $currentRequest = $this->createMock(Request::class);

        $this->requestStack
            ->expects($this->once())
            ->method('getCurrentRequest')
            ->willReturn($currentRequest);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getAction')
            ->with($currentRequest)
            ->willReturn(FormSetParentEventListener::ACTION_CREATE_ISSUE_SUBTASK);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getEntityClassName')
            ->with($currentRequest)
            ->willReturn(Issue::class);

        $this->entityRoutingHelper
            ->expects($this->once())
            ->method('getEntityId')
            ->with($currentRequest)
            ->willReturn(1);

        $this->entityRoutingHelper->expects($this->once())
            ->method('getEntity')
            ->with(Issue::class, 1)
            ->willReturn(new Issue());

        $form = $this->createMock(FormInterface::class);
        $parentForm = $this->createMock(FormInterface::class);
        $typeForm = $this->createMock(FormInterface::class);

        $parentFormConfig = $this->createMock(FormConfigInterface::class);
        $typeFormConfig = $this->createMock(FormConfigInterface::class);

        $parentFormConfigType = $this->createMock(ResolvedFormTypeInterface::class);
        $typeFormConfigType = $this->createMock(ResolvedFormTypeInterface::class);

        $formSelectConfigInnerType = $this->createMock(Select2EntityType::class);
        $formChoiserConfigInnerType = $this->createMock(ChoiceType::class);

        $parentFormConfigType->expects($this->once())
            ->method('getInnerType')
            ->willReturn($formSelectConfigInnerType);

        $parentFormConfig->expects($this->any())
            ->method('getOptions')
            ->willReturn([
                'attr' => ['readonly' => false]
            ]);

        $parentFormConfig->expects($this->once())
            ->method('getType')
            ->willReturn($parentFormConfigType);

        $parentForm->expects($this->once())
            ->method('getConfig')
            ->willReturn($parentFormConfig);

        $typeFormConfigType->expects($this->once())
            ->method('getInnerType')
            ->willReturn($formChoiserConfigInnerType);

        $typeFormConfig->expects($this->any())
            ->method('getOptions')
            ->willReturn([
                'attr' => ['readonly' => false]
            ]);

        $typeFormConfig->expects($this->once())
            ->method('getType')
            ->willReturn($typeFormConfigType);

        $typeForm->expects($this->once())
            ->method('getConfig')
            ->willReturn($typeFormConfig);

        $form->expects($this->exactly(2))
            ->method('get')
            ->will($this->returnValueMap([
                ['parent', $parentForm],
                ['type', $typeForm]
            ]));

        $form->method('add')
            ->withConsecutive(
                ['parent', get_class($formSelectConfigInnerType), ['attr' => ['readonly' => true]]],
                ['type', get_class($formChoiserConfigInnerType), ['attr' => ['readonly' => true]]]
            );

        $issue = new Issue();
        $event = $this->createMock(FormProcessEvent::class);
        $event->expects($this->once())
            ->method('getData')
            ->willReturn($issue);

        $event->expects($this->once())
            ->method('getForm')
            ->willReturn($form);

        $this->listener->setParentAndLockForm($event);

        self::assertInstanceOf(Issue::class, $issue->getParent());
        self::assertEquals(IssueTypeProvider::SUBTASK_TYPE, $issue->getType());
    }
}

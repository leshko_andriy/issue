<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\EventListener\IssueReporterEventListener;
use Oro\Bundle\UserBundle\Entity\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Unit test doctrine listener auto add reporter and collaborators
 */
class IssueReporterEventListenerTest extends TestCase
{
    /**
     * @var EntityManager|MockObject
     */
    protected $em;

    /**
     * @var TokenStorage|MockObject
     */
    protected $tokenStorage;

    /**
     * @var IssueReporterEventListener
     */
    protected $listener;

    protected function setUp(): void
    {
        $this->em  = $this->createMock(EntityManager::class);

        $user = $this->createMock(User::class);

        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())->method('getUser')->will($this->returnValue($user));

        $this->tokenStorage = $this->createMock(TokenStorage::class);
        $this->tokenStorage->expects($this->any())->method('getToken')->will($this->returnValue($token));

        $this->listener = new IssueReporterEventListener($this->tokenStorage);
    }

    public function testPrePersist(): void
    {
        $entity = new Issue();
        $args = new LifecycleEventArgs($entity, $this->em);

        $this->listener->prePersist($entity, $args);
        self::assertInstanceOf(User::class, $entity->getReporter());
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Validator;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\IssueBundle\Validator\StorySubtaskValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

/**
 * unit test for validator which describe dependency between story and subtask types
 */
class StorySubtaskValidatorTest extends TestCase
{
    /** @var Constraint */
    private $constraint;

    protected function setUp()
    {
        $this->constraint = $this->createMock(Constraint::class);
    }

    /**
     * @throws \ReflectionException
     */
    public function testNotChildValid()
    {
        $issueChild = new Issue();
        $issueChild->setType(IssueTypeProvider::STORY_TYPE);
        $issue = new Issue();
        $issue->setType(IssueTypeProvider::STORY_TYPE);
        $issue->addChild($issueChild);

        $context = $this->createMock(ExecutionContext::class);
        $this->constraint->message = 'oro.issue.validation.child_message';
        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $context->expects($this->once())
            ->method('buildViolation')
            ->with($this->constraint->message)
            ->willReturn($builder);
        $builder->expects($this->once())
            ->method('setTranslationDomain')
            ->with('validators')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('atPath')
            ->with('type')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('addViolation');

        $validator = new StorySubtaskValidator();
        $validatorReflection = new \ReflectionClass($validator);
        $propertyContext = $validatorReflection->getProperty('context');
        $propertyContext->setAccessible(true);
        $propertyContext->setValue($validator, $context);
        $validator->validate($issue, $this->constraint);
    }

    /**
     * @throws \ReflectionException
     */
    public function testNotStoryValid()
    {
        $issueChild = new Issue();
        $issueChild->setType(IssueTypeProvider::SUBTASK_TYPE);
        $issue = new Issue();
        $issue->setType(IssueTypeProvider::TASK_TYPE);
        $issue->addChild($issueChild);

        $context = $this->createMock(ExecutionContext::class);
        $this->constraint->message = 'oro.issue.validation.story_message';
        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $context->expects($this->once())
            ->method('buildViolation')
            ->with($this->constraint->message)
            ->willReturn($builder);
        $builder->expects($this->once())
            ->method('setTranslationDomain')
            ->with('validators')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('atPath')
            ->with('type')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('addViolation');

        $validator = new StorySubtaskValidator();
        $validatorReflection = new \ReflectionClass($validator);
        $propertyContext = $validatorReflection->getProperty('context');
        $propertyContext->setAccessible(true);
        $propertyContext->setValue($validator, $context);
        $validator->validate($issue, $this->constraint);
    }

    /**
     * @throws \ReflectionException
     */
    public function testNotSubtaskValid()
    {
        $issueParent = new Issue();
        $issueParent->setType(IssueTypeProvider::STORY_TYPE);
        $issue = new Issue();
        $issue->setType(IssueTypeProvider::TASK_TYPE);
        $issue->setParent($issueParent);

        $context = $this->createMock(ExecutionContext::class);
        $this->constraint->message = 'oro.issue.validation.story_message';
        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $context->expects($this->once())
            ->method('buildViolation')
            ->with($this->constraint->message)
            ->willReturn($builder);
        $builder->expects($this->once())
            ->method('setTranslationDomain')
            ->with('validators')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('atPath')
            ->with('type')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('addViolation');

        $validator = new StorySubtaskValidator();
        $validatorReflection = new \ReflectionClass($validator);
        $propertyContext = $validatorReflection->getProperty('context');
        $propertyContext->setAccessible(true);
        $propertyContext->setValue($validator, $context);
        $validator->validate($issue, $this->constraint);
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Validator;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\IssueBundle\Validator\SubtaskHasParentValidator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

/**
 * unit test for validator which check subtask must have parent
 */
class SubtaskHasParentValidatorTest extends TestCase
{
    /** @var Constraint */
    private $constraint;

    /** @var ConstraintViolationBuilderInterface|MockObject */
    private $context;

    protected function setUp()
    {
        $this->constraint = $this->createMock(Constraint::class);
        $this->constraint->message = 'oro.issue.validation.parent_message';
        $this->context = $this->createMock(ExecutionContext::class);
        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $this->context->expects($this->once())
            ->method('buildViolation')
            ->with($this->constraint->message)
            ->willReturn($builder);
        $builder->expects($this->once())
            ->method('setTranslationDomain')
            ->with('validators')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('atPath')
            ->with('parent')
            ->willReturnSelf();
        $builder->expects($this->once())
            ->method('addViolation');
    }

    /**
     * @throws \ReflectionException
     */
    public function testNotValid()
    {
        $issue = new Issue();
        $issue->setType(IssueTypeProvider::SUBTASK_TYPE);

        $validator = new SubtaskHasParentValidator();
        $validatorReflection = new \ReflectionClass($validator);
        $propertyContext = $validatorReflection->getProperty('context');
        $propertyContext->setAccessible(true);
        $propertyContext->setValue($validator, $this->context);
        $validator->validate($issue, $this->constraint);
    }
}

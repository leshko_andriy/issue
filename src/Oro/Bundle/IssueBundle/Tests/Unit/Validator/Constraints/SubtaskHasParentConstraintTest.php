<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Validator\Constraints;

use Oro\Bundle\IssueBundle\Validator\Constraints\SubtaskHasParentConstraint;
use Oro\Bundle\IssueBundle\Validator\SubtaskHasParentValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

/**
 * unit test for constraint which check subtask must have parent
 */
class SubtaskHasParentConstraintTest extends TestCase
{
    /** @var SubtaskHasParentConstraint */
    protected $constraint;

    protected function setUp()
    {
        $this->constraint = new SubtaskHasParentConstraint();
    }

    public function testConfiguration()
    {
        self::assertEquals(
            SubtaskHasParentValidator::class,
            $this->constraint->validatedBy()
        );
        self::assertEquals('oro.issue.validation.parent_message', $this->constraint->message);
        self::assertEquals(Constraint::CLASS_CONSTRAINT, $this->constraint->getTargets());
    }
}

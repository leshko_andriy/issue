<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Validator\Constraints;

use Oro\Bundle\IssueBundle\Validator\Constraints\StorySubtaskConstraint;
use Oro\Bundle\IssueBundle\Validator\StorySubtaskValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

/**
 * unit test for constraint which describe dependency between story and subtask types
 */
class StorySubtaskConstraintTest extends TestCase
{
    /** @var StorySubtaskConstraint */
    protected $constraint;

    protected function setUp()
    {
        $this->constraint = new StorySubtaskConstraint();
    }

    public function testConfiguration()
    {
        self::assertEquals(
            StorySubtaskValidator::class,
            $this->constraint->validatedBy()
        );
        self::assertEquals('oro.issue.validation.child_message', $this->constraint->messageChild);
        self::assertEquals('oro.issue.validation.story_message', $this->constraint->messageStory);
        self::assertEquals('oro.issue.validation.subtask_message', $this->constraint->messageSubtask);
        self::assertEquals(Constraint::CLASS_CONSTRAINT, $this->constraint->getTargets());
    }
}

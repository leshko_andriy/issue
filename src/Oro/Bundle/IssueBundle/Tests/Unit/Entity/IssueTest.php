<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Entity;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Component\Testing\Unit\EntityTestCaseTrait;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for entity issue
 */
class IssueTest extends TestCase
{
    use EntityTestCaseTrait;

    /**
     * @throws \Exception
     */
    public function testProps(): void
    {
        $properties = [
            ['id', 1],
            ['code', 'T-52'],
            ['summary', 'Test Summary'],
            ['description', 'Test Description'],
            ['type', IssueTypeProvider::SUBTASK_TYPE],
            ['parent', new Issue()],
            ['assignee', new User()],
            ['reporter', new User()],
            ['organization', new Organization()],
            ['updatedAt', new \DateTime('now', new \DateTimeZone('UTC'))],
            ['createdAt', new \DateTime('now', new \DateTimeZone('UTC'))],
        ];

        self::assertPropertyAccessors(new Issue(), $properties);
    }

    public function testCollections(): void
    {
        $collections = [
            ['collaborators', new User()],
            ['relatedIssues', new Issue()],
            ['children', new Issue()],
            ['relatedWith', new Issue()],
        ];

        self::assertPropertyCollections(new Issue(), $collections);
    }

    public function testGetChildCodes(): void
    {
        $issue = new Issue();
        $issue2 = new Issue();
        $issue2->setCode('a2');
        $issue3 = new Issue();
        $issue3->setCode('a3');

        $issue->addChild($issue2);
        $issue->addChild($issue3);

        self::assertEquals(['a2','a3'], $issue->getChildCodes());
    }

    public function testSyncRelateIssue(): void
    {
        $issue1 = new Issue();
        $issue2 = new Issue();

        $issue1->addRelatedIssue($issue2);

        self::assertEquals($issue2->getRelatedIssues()[0], $issue1);

        $issue2->removeRelatedIssue($issue1);

        self::assertEquals(count($issue1->getRelatedIssues()), 0);
    }

    public function testAddCollaborators(): void
    {
        $issue1 = new Issue();
        $issue2 = new Issue();
        $user1 = new User();
        $user2 = new User();

        $issue1->setAssignee($user1);

        self::assertEquals($issue1->getCollaborators()[0], $user1);

        $issue2->setReporter($user2);

        self::assertEquals($issue2->getCollaborators()[0], $user2);
    }
}

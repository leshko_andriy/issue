<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Form;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Form\IssueFormTemplateDataProvider;
use Oro\Component\Testing\Unit\EntityTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Test form template data handler
 */
class IssueFormTemplateDataProviderTest extends TestCase
{
    use EntityTrait;

    /** @var RouterInterface|MockObject */
    private $router;

    /** @var IssueFormTemplateDataProvider */
    private $provider;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->router = $this->createMock(RouterInterface::class);
        $this->provider = new IssueFormTemplateDataProvider($this->router);
    }

    /**
     * {@inheritdoc}
     */
    public function testDataWithEntityId(): void
    {
        $issue = $this->getEntity(Issue::class, [
            'id' => 1,
            'summary' => 'test'
        ]);

        $request = new Request();

        $this->router->expects($this->once())
            ->method('generate')
            ->with('oro_issue_update', ['id' => 1])
            ->willReturn('/update/1');

        $formView = $this->createMock(FormView::class);

        $form = $this->createMock(FormInterface::class);
        $form->expects($this->any())
            ->method('createView')
            ->willReturn($formView);

        $result = $this->provider->getData($issue, $form, $request);

        self::assertArrayHasKey('entity', $result);
        self::assertEquals($issue, $result['entity']);
        self::assertArrayHasKey('form', $result);
        self::assertEquals($formView, $result['form']);
        self::assertArrayHasKey('formAction', $result);
        self::assertEquals('/update/1', $result['formAction']);
    }

    /**
     * {@inheritdoc}
     */
    public function testDataWithoutEntityId(): void
    {
        $issue = new Issue();

        $request = new Request();

        $this->router->expects($this->once())
            ->method('generate')
            ->with('oro_issue_create')
            ->willReturn('/create');

        $formView = $this->createMock(FormView::class);

        $form = $this->createMock(FormInterface::class);
        $form->expects($this->any())
            ->method('createView')
            ->willReturn($formView);

        $result = $this->provider->getData($issue, $form, $request);

        self::assertArrayHasKey('entity', $result);
        self::assertEquals($issue, $result['entity']);
        self::assertArrayHasKey('form', $result);
        self::assertEquals($formView, $result['form']);
        self::assertArrayHasKey('formAction', $result);
        self::assertEquals('/create', $result['formAction']);
    }
}

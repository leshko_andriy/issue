<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Form\Type;

use Oro\Bundle\EmbeddedFormBundle\Form\Type\EmbeddedFormInterface;
use Oro\Bundle\EntityExtendBundle\Form\Type\EnumChoiceType;
use Oro\Bundle\IssueBundle\Form\Type\IssueEmbeddedType;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\IssueBundle\Tests\Unit\Stub\IssueStub;
use Oro\Component\Testing\Unit\Entity\Stub\StubEnumValue;
use Oro\Component\Testing\Unit\Form\Type\Stub\ChoiceLoaderStub;
use Oro\Component\Testing\Unit\Form\Type\Stub\EnumSelectType as EnumSelectTypeStub;
use Oro\Component\Testing\Unit\FormIntegrationTestCase;
use Oro\Component\Testing\Unit\PreloadedExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Test embedded issue form
 */
class IssueEmbeddedTypeTest extends FormIntegrationTestCase
{
    /**
     * @var IssueEmbeddedType
     */
    private $embeddedForm;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        /** @var TranslatorInterface $trans */
        $trans = $this->createMock(TranslatorInterface::class);
        $trans->expects($this->any())->method('trans')->willReturn('success');

        $this->embeddedForm = new IssueEmbeddedType($trans);
        parent::setUp();
    }

    /**
     * @return array
     */
    protected function getExtensions(): array
    {
        return [
            new PreloadedExtension(
                [
                    TextareaType::class => new TextareaType(),
                    TextType::class => new TextType(),
                    EnumChoiceType::class => new EnumSelectTypeStub([
                        new StubEnumValue(
                            IssuePriorityProvider::PRIORITY_BLOCKER,
                            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER]
                        ),
                        new StubEnumValue(
                            IssuePriorityProvider::PRIORITY_MAJOR,
                            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_MAJOR]
                        )
                    ]),
                    ChoiceType::class => new ChoiceLoaderStub(IssueTypeProvider::TYPES),
                    IssueEmbeddedType::class => $this->embeddedForm
                ],
                []
            ),
            $this->getValidatorExtension(false),
        ];
    }

    /**
     * @dataProvider submitDataProvider
     *
     * @param IssueStub $defaultData
     * @param array $submittedData
     * @param IssueStub $expectedData
     * @throws \Exception
     */
    public function testSubmit(
        IssueStub $defaultData,
        array $submittedData,
        IssueStub $expectedData
    ): void {
        $form = $this->factory->create(IssueEmbeddedType::class, $defaultData);

        self::assertEquals($defaultData, $form->getData());

        $formFields = $form->createView()->children;

        self::assertArrayHasKey('submit', $formFields);

        $form->submit($submittedData);

        self::assertTrue($form->isSubmitted());
        self::assertTrue($form->isSynchronized());
        self::assertTrue($form->isValid(), $form->getErrors(true, false));

        static::assertEquals($expectedData, $form->getData());
    }

    public function testNeedOptionEmbeddedForm(): void
    {
        self::assertInstanceOf(EmbeddedFormInterface::class, $this->embeddedForm);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function submitDataProvider(): array
    {
        $defaultIssue = new IssueStub();
        $defaultIssue->setSummary('Old summary');
        $defaultIssue->setDescription('Old description');
        $defaultIssue->setPriority(new StubEnumValue(
            IssuePriorityProvider::PRIORITY_BLOCKER,
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER]
        ));

        $defaultIssue->setType(IssueTypeProvider::BUG_TYPE);

        $expectedIssue = new IssueStub();
        $expectedIssue->setSummary('New summary');
        $expectedIssue->setDescription('New description');
        $expectedIssue->setPriority(new StubEnumValue(
            IssuePriorityProvider::PRIORITY_MAJOR,
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_MAJOR]
        ));

        $expectedIssue->setType(IssueTypeProvider::STORY_TYPE);

        return [
            'issue update' => [
                'defaultData' => $defaultIssue,
                'submittedData' => [
                    'summary' => 'New summary',
                    'description' => 'New description',
                    'priority' => IssuePriorityProvider::PRIORITY_MAJOR,
                    'type' => IssueTypeProvider::STORY_TYPE,
                ],
                'expectedData' => $expectedIssue
            ]
        ];
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Form\Type;

use Oro\Bundle\EntityExtendBundle\Form\Type\EnumChoiceType;
use Oro\Bundle\IssueBundle\Form\Type\IssueAssigneeReporterType;
use Oro\Bundle\IssueBundle\Form\Type\IssueEntityType;
use Oro\Bundle\IssueBundle\Form\Type\IssueType;
use Oro\Bundle\IssueBundle\Form\Type\IssueUserType;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\IssueBundle\Tests\Unit\Stub\IssueStub;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Component\Testing\Unit\Entity\Stub\StubEnumValue;
use Oro\Component\Testing\Unit\EntityTrait;
use Oro\Component\Testing\Unit\Form\Type\Stub\ChoiceLoaderStub;
use Oro\Component\Testing\Unit\Form\Type\Stub\EntityType;
use Oro\Component\Testing\Unit\Form\Type\Stub\EnumSelectType as EnumSelectTypeStub;
use Oro\Component\Testing\Unit\FormIntegrationTestCase;
use Oro\Component\Testing\Unit\PreloadedExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class IssueTypeTest. Test main issue form
 */
class IssueTypeTest extends FormIntegrationTestCase
{
    use EntityTrait;

    /** @var User */
    private static $user1;

    /** @var User */
    private static $user2;

    /** @var IssueStub */
    private static $issue1;

    /** @var IssueStub */
    private static $issue2;
    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return array
     */
    protected function getExtensions(): array
    {
        return [
            new PreloadedExtension(
                [
                    TextareaType::class => new TextareaType(),
                    TextType::class => new TextType(),
                    EnumChoiceType::class => new EnumSelectTypeStub([
                        new StubEnumValue(
                            IssuePriorityProvider::PRIORITY_BLOCKER,
                            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER]
                        ),
                        new StubEnumValue(
                            IssuePriorityProvider::PRIORITY_MAJOR,
                            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_MAJOR]
                        )
                    ]),
                    ChoiceType::class => new ChoiceLoaderStub(IssueTypeProvider::TYPES),
                    IssueUserType::class => new EntityType([
                        1 => self::$user1,
                        2 => self::$user2
                    ], IssueAssigneeReporterType::class),
                    IssueEntityType::class => new EntityType([
                        1 => self::$issue1,
                        2 => self::$issue2
                    ], IssueEntityType::class),
                ],
                []
            ),
            $this->getValidatorExtension(false),
        ];
    }

    /**
     * @dataProvider submitDataProvider
     *
     * @param IssueStub $defaultData
     * @param array $submittedData
     * @param IssueStub $expectedData
     * @throws \Exception
     */
    public function testSubmit(
        IssueStub $defaultData,
        array $submittedData,
        IssueStub $expectedData
    ): void {
        $form = $this->factory->create(IssueType::class, $defaultData);

        self::assertEquals($defaultData, $form->getData());

        $form->submit($submittedData);

        self::assertTrue($form->isSubmitted());
        self::assertTrue($form->isSynchronized());
        self::assertTrue($form->isValid(), $form->getErrors(true, false));

        static::assertEquals($expectedData, $form->getData());
    }

    //public function test

    /**
     * @return array
     * @throws \Exception
     */
    public function submitDataProvider(): array
    {
        self::$issue1 = $this->getTmpIssue(1);
        self::$issue2 = $this->getTmpIssue(2);

        self::$user1 = $this->getTmpUser(1);
        self::$user2 = $this->getTmpUser(2);

        $defaultIssue = new IssueStub();
        $defaultIssue->setSummary('Old summary');
        $defaultIssue->setDescription('Old description');
        $defaultIssue->setPriority(new StubEnumValue(
            IssuePriorityProvider::PRIORITY_BLOCKER,
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER]
        ));
        $defaultIssue->setType(IssueTypeProvider::BUG_TYPE);

        $expectedIssue = new IssueStub();
        $expectedIssue->setSummary('New summary');
        $expectedIssue->setDescription('New description');
        $expectedIssue->setPriority(new StubEnumValue(
            IssuePriorityProvider::PRIORITY_MAJOR,
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_MAJOR]
        ));
        $expectedIssue->setType(IssueTypeProvider::SUBTASK_TYPE);
        $expectedIssue->setParent(self::$issue2);
        $expectedIssue->setAssignee(self::$user2);
        $expectedIssue->addRelatedIssue(self::$issue1);
        $expectedIssue->addRelatedIssue(self::$issue2);

        return [
            'issue update' => [
                'defaultData' => $defaultIssue,
                'submittedData' => [
                    'summary' => 'New summary',
                    'description' => 'New description',
                    'priority' => IssuePriorityProvider::PRIORITY_MAJOR,
                    'assignee' => '2',
                    'type' => IssueTypeProvider::SUBTASK_TYPE,
                    'parent' => '2',
                    'relatedIssues' => ['1','2']
                ],
                'expectedData' => $expectedIssue
            ]
        ];
    }

    /**
     * @param $id
     * @return User
     */
    private function getTmpUser($id): User
    {
        return $this->getEntity(User::class, [
            'id' => $id,
            'salt' => $id,
            'firstName' => 'admin',
            'lastName' => 'admin',
        ]);
    }

    /**
     * @param $id
     * @return IssueStub
     */
    private function getTmpIssue($id): IssueStub
    {
        return $this->getEntity(IssueStub::class, [
            'id' => $id,
            'code' => 'T-' . $id,
            'summary' => 'test'
        ]);
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Provider;

use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for providers
 */
class IssueProviderTest extends TestCase
{
    public function testPriorityProvider(): void
    {
        self::assertEquals([
            'blocker' => 'Blocker',
            'critical' => 'Critical',
            'major' => 'Major',
            'trivial' => 'Trivial',
            'minor' => 'Minor',
        ], IssuePriorityProvider::PRIORITY_FIELDS);
    }

    public function testStatusProvider(): void
    {
        self::assertEquals([
            'open' => 'Open',
            'in_progress' => 'In progress',
            'resolved' => 'Resolved',
            'reopened' => 'Reopen',
            'closed' => 'Closed'
        ], IssueStatusProvider::STATUS_FIELDS);
    }

    public function testTypeProvider(): void
    {
        self::assertEquals([
            'SubTask' => 'SubTask',
            'Bug' => 'Bug',
            'Story' => 'Story',
            'Task' => 'Task'
        ], IssueTypeProvider::TYPES);
    }

    public function testResolutionProvider(): void
    {
        self::assertEquals([
            'done' => 'Done',
            'fixed' => 'Fixed',
            'duplicate' => 'Duplicate',
            'incomplete' => 'Incomplete',
            'wontfix' => 'WontFix',
            'failed' => 'Failed',
            'declined' => 'Declined'
        ], IssueResolutionProvider::RESOLUTION_FIELDS);
    }
}

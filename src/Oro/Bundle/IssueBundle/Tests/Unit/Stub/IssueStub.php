<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\Stub;

use Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue;
use Oro\Bundle\IssueBundle\Entity\Issue;

/**
 * Help class Issue for testing
 */
class IssueStub extends Issue
{
    /** @var AbstractEnumValue */
    private $priority;

    /**
     * @return AbstractEnumValue
     */
    public function getPriority(): AbstractEnumValue
    {
        return $this->priority;
    }

    /**
     * @param AbstractEnumValue $priority
     */
    public function setPriority(AbstractEnumValue $priority): void
    {
        $this->priority = $priority;
    }
}

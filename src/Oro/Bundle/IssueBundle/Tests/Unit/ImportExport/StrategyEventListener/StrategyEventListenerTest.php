<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\ImportExport\StrategyEventListener;

use Doctrine\ORM\EntityManager;
use Oro\Bundle\ApiBundle\Util\DoctrineHelper;
use Oro\Bundle\ImportExportBundle\Context\ContextInterface;
use Oro\Bundle\ImportExportBundle\Event\StrategyEvent;
use Oro\Bundle\ImportExportBundle\Strategy\StrategyInterface;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository;
use Oro\Bundle\IssueBundle\ImportExport\StrategyEventListener\StrategyEventListener;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for listener strategy
 */
class StrategyEventListenerTest extends TestCase
{
    /**
     * @var DoctrineHelper | MockBuilder
     */
    protected $dh;

    /**
     * @var StrategyEventListener
     */
    protected $listener;

    /**
     * @var StrategyInterface
     */
    protected $strategy;

    /**
     * @var ContextInterface
     */
    protected $context;

    protected function setUp(): void
    {
        $this->dh  = $this->getMockBuilder(DoctrineHelper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->createMock(ContextInterface::class);

        $this->strategy = $this->createMock(StrategyInterface::class);

        $this->listener = new StrategyEventListener($this->dh);
    }

    public function testBeforeImportNotPostponed(): void
    {
        $repo = $this->createMock(IssueRepository::class);

        $issue = new Issue();
        $issue2 = new Issue();
        $issue2->setCode('T-2');
        $issue->addChild($issue2);

        $repo->expects($this->exactly(1))
            ->method('findBy')
            ->with(['code' => ['T-2']])
            ->will($this->returnValue([$issue2]));

        $em = $this->createMock(EntityManager::class);

        $em
            ->expects($this->once())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $this->dh
            ->expects($this->once())
            ->method('getEntityManager')
            ->will($this->returnValue($em));

        $event = new StrategyEvent($this->strategy, $issue, $this->context);

        $this->listener->beforeImportStrategy($event);

        self::assertInstanceOf(Issue::class, $event->getEntity());
    }

    public function testBeforeImportPostponed(): void
    {
        $repo = $this->createMock(IssueRepository::class);

        $repo->expects($this->exactly(1))
            ->method('findBy')
            ->with(['code' => ['T-3']])
            ->will($this->returnValue([]));

        $em = $this->createMock(EntityManager::class);

        $em
            ->expects($this->once())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $this->dh
            ->expects($this->once())
            ->method('getEntityManager')
            ->will($this->returnValue($em));

        $issue = new Issue();
        $issue2 = new Issue();
        $issue2->setCode('T-3');
        $issue->addChild($issue2);

        $this->context
            ->expects($this->once())
            ->method('getValue')
            ->will($this->returnValue(['row' => 'some_item']));

        $this->context
            ->expects($this->once())
            ->method('addPostponedRow')
            ->with(['row' => 'some_item']);

        $event = new StrategyEvent($this->strategy, $issue, $this->context);

        $this->listener->beforeImportStrategy($event);

        self::assertNull($event->getEntity());
    }
}

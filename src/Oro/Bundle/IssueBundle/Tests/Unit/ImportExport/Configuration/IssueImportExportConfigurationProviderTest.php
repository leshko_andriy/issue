<?php

namespace Oro\Bundle\IssueBundle\Tests\Unit\ImportExport\Configuration;

use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfiguration;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\ImportExport\Configuration\IssueImportExportConfigurationProvider;
use PHPUnit\Framework\TestCase;

/**
 * Unit test config import export
 */
class IssueImportExportConfigurationProviderTest extends TestCase
{
    public function testGet()
    {
        static::assertEquals(
            new ImportExportConfiguration([
                ImportExportConfiguration::FIELD_ENTITY_CLASS => Issue::class,
                ImportExportConfiguration::FIELD_EXPORT_TEMPLATE_PROCESSOR_ALIAS => 'oro_issue',
                ImportExportConfiguration::FIELD_EXPORT_PROCESSOR_ALIAS => 'oro_issue',
                ImportExportConfiguration::FIELD_IMPORT_PROCESSOR_ALIAS => 'oro_issue',
            ]),
            (new IssueImportExportConfigurationProvider())->get()
        );
    }
}

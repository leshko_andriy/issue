<?php

namespace Oro\Bundle\IssueBundle\Tests\Functional\EventListener;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository;
use Oro\Bundle\IssueBundle\Form\Type\IssueType;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\TestFrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;

/**
 * Test issue form listener. pre set data. Modify field parent and relatedIssues.
 */
class IssueTypeSubscribeTest extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->initClient();
        $this->loadFixtures(
            [
                '@OroIssueBundle/Tests/Functional/DataFixtures/issue_data.yml'
            ]
        );
        $this->container = static::getContainer();
    }

    public function testGetCountStatusesWithNames(): void
    {
        /** @var FormFactory $formFactory */
        $formFactory = $this->container->get('form.factory');

        /** @var Form $form */
        $form = $formFactory->create(IssueType::class);

        /** @var Issue $issue */
        $issue = $this->getReference('reference_issue2');
        $form->setData($issue);

        $view = $form->createView();

        $parentData = $this->collectIdFromData($view->children['parent']->vars['choices']);
        $relatedData = $this->collectIdFromData($view->children['relatedIssues']->vars['choices']);

        /** @var IssueRepository $repo */
        $repo = $this->container->get('doctrine')->getrepository(Issue::class);

        self::assertNotContains($issue->getId(), $relatedData);
        self::assertNotContains($issue->getId(), $parentData);

        /** @var Issue[] $parents */
        $parents = $repo->findBy(['id' => $parentData]);

        foreach ($parents as $parent) {
            self::assertEquals($parent->getType(), IssueTypeProvider::STORY_TYPE);
        }
    }

    /**
     * @param array $collection
     * @return array
     */
    private function collectIdFromData(array $collection): array
    {
        $results = [];
        foreach ($collection as $item) {
            $results[] = $item->value;
        }

        return $results;
    }
}

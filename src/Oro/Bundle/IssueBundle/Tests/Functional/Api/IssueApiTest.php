<?php

namespace Oro\Bundle\IssueBundle\Tests\Functional\Api;

use Doctrine\ORM\EntityManager;
use Oro\Bundle\ApiBundle\Tests\Functional\RestJsonApiTestCase;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * Functional test api issue crud
 */
class IssueApiTest extends RestJsonApiTestCase
{
    private const ISSUE_RESOURCES = [
        'status', 'priority', 'resolution', 'assignee', 'reporter', 'parent',
        'children', 'collaborators', 'relatedIssues', 'relatedWith', 'organization'
    ];

    private const ISSUE_PATCH_RESOURCES = [
        'status', 'priority', 'resolution', 'assignee', 'parent', 'organization'
    ];

    /**
     * @var Issue $issue
     */
    protected $issue;

    /**
     * @var Issue $issue2
     */
    protected $issue2;

    protected function setUp(): void
    {
        $fixtures = [
            '@OroIssueBundle/Tests/Functional/Api/DataFixtures/issue_data.yml'
        ];

        parent::setUp();
        $this->loadFixtures($fixtures);

        $this->issue = $this->getReference('reference_issue1');
        $this->issue2 = $this->getReference('reference_issue2');
    }

    public function testGetList(): void
    {
        $response = $this->cget(['entity' => 'issues'], []);
        $this->assertResponseContains('issue_cget.yml', $response);
    }

    /**
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testCreate(): void
    {
        $this->post(
            ['entity' => 'issues'],
            'issue_create.yml'
        );

        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        /** @var Issue $issue */
        $issue = $em->getRepository(Issue::class)->findOneBy([
            'summary' => 'Summary of test issue'
        ]);

        self::assertEquals('Summary of test issue', $issue->getSummary());
        self::assertEquals('Description of test issue', $issue->getDescription());
        self::assertEquals(
            IssueStatusProvider::STATUS_FIELDS[IssueStatusProvider::STATUS_OPEN],
            $issue->getStatus()->getName()
        );
        self::assertEquals(IssueTypeProvider::BUG_TYPE, $issue->getType());
        self::assertEquals(
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_MAJOR],
            $issue->getPriority()->getName()
        );
        self::assertEquals(
            1,
            $issue->getAssignee()->getId()
        );

        $em->remove($issue);
        $em->flush();
        $em->clear();
    }

    public function testGet(): void
    {
        $response = $this->get(['entity' => 'issues', 'id' => $this->issue->getId()]);
        $this->assertResponseContains('issue_get.yml', $response);
    }

    public function testUpdate(): void
    {
        $response = $this->patch(
            ['entity' => 'issues', 'id' => (string)$this->issue->getId()],
            'issue_update.yml'
        );
        $this->assertResponseContains('issue_patch.yml', $response);

        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        /** @var Issue $issue */
        $issue = $em->getRepository(Issue::class)->findOneBy([
            'summary' => 'Summary new of test issue'
        ]);

        self::assertEquals('Summary new of test issue', $issue->getSummary());
        self::assertEquals('Description new of test issue', $issue->getDescription());
        self::assertEquals(
            IssueStatusProvider::STATUS_FIELDS[IssueStatusProvider::STATUS_CLOSED],
            $issue->getStatus()->getName()
        );
        self::assertEquals(IssueTypeProvider::STORY_TYPE, $issue->getType());
        self::assertEquals(
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER],
            $issue->getPriority()->getName()
        );
        self::assertEquals(
            IssueResolutionProvider::RESOLUTION_FIELDS[IssueResolutionProvider::RESOLUTION_DONE],
            $issue->getResolution()->getName()
        );
        self::assertEquals(
            1,
            $issue->getAssignee()->getId()
        );
    }

    public function testGetRelationship(): void
    {
        foreach (self::ISSUE_RESOURCES as $resource) {
            $issueId = $this->issue->getId();
            if ($resource === 'parent' || $resource === 'relatedIssues') {
                $issueId = $this->issue2->getId();
            }
            $this->assertGetRelationShip(
                $issueId,
                $resource
            );
        }
    }

    public function testGetSubResource(): void
    {
        foreach (self::ISSUE_RESOURCES as $resource) {
            $issueId = $this->issue->getId();
            if ($resource === 'parent' || $resource === 'relatedIssues') {
                $issueId = $this->issue2->getId();
            }
            $this->assertGetSubresource(
                $issueId,
                $resource
            );
        }
    }

    public function testResourcePatch(): void
    {
        foreach (self::ISSUE_PATCH_RESOURCES as $resource) {
            $this->assertPatchRelationShip($this->issue2->getId(), $resource);
        }

        $response = $this->get(['entity' => 'issues', 'id' => $this->issue2->getId()]);
        $this->assertResponseContains('patch/issue_get.yml', $response);
    }

    public function testDeleteRelatedIssues(): void
    {
        $this->deleteRelationship(
            ['entity' => 'issues', 'id' => $this->issue2->getId(), 'association' => 'relatedIssues'],
            $this->getRequestData('delete/issue_relatedIssues.yml'),
            [],
            true
        );
    }

    public function testAddRelatedIssues(): void
    {
        $this->postRelationship(
            ['entity' => 'issues', 'id' => $this->issue2->getId(), 'association' => 'relatedIssues'],
            $this->getRequestData('delete/issue_relatedIssues.yml'),
            [],
            true
        );
    }

    public function testDelete(): void
    {
        $this->delete(['entity' => 'issues', 'id' => (string)$this->issue->getId()]);
    }

    /**
     * @param int    $entityId
     * @param string $associationName
     *
     * @return Response
     */
    private function assertGetSubResource(int $entityId, string $associationName): Response
    {
        $response = $this->getSubresource([
            'id' => $entityId,
            'entity' => 'issues',
            'association' => $associationName
        ]);
        $this->assertResponseContains('subresource/issue_' . $associationName . '_get.yml', $response);

        return $response;
    }

    /**
     * @param int    $entityId
     * @param string $associationName
     *
     * @return Response
     */
    private function assertGetRelationShip(int $entityId, string $associationName): Response
    {
        $response = $this->getRelationship([
            'id' => $entityId,
            'entity' => 'issues',
            'association' => $associationName
        ]);
        $this->assertResponseContains('relationship/issue_' . $associationName . '_get.yml', $response);

        return $response;
    }

    /**
     * @param int    $entityId
     * @param string $associationName
     * @param bool   $assertValid
     *
     * @return Response
     */
    private function assertPatchRelationShip(
        int $entityId,
        string $associationName,
        bool $assertValid = true
    ): Response {
        return $this->patchRelationship(
            ['entity' => 'issues', 'id' => $entityId, 'association' => $associationName],
            $this->getRequestData('patch/issue_' . $associationName . '.yml'),
            [],
            $assertValid
        );
    }
}

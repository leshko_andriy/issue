<?php

namespace Oro\Bundle\IssueBundle\Tests\Functional\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;

/**
 * Set priority and status object to reference repo
 */
class LoadPriorityResolutionAndStatusData extends AbstractFixture
{
    public const ISSUE_PRIORITY_MAJOR = 'issue_priority_major';
    public const ISSUE_PRIORITY_BLOCKER = 'issue_priority_blocker';
    public const ISSUE_STATUS_OPEN = 'issue_status_open';
    public const ISSUE_STATUS_CLOSE = 'issue_status_closed';
    public const ISSUE_RESOLUTION_DONE = 'issue_resolution_done';
    public const ISSUE_RESOLUTION_FAILED = 'issue_resolution_failed';

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager): void
    {
        $classPriorityName = ExtendHelper::buildEnumValueClassName(
            IssuePriorityProvider::ENUM_CODE_PRIORITY
        );
        $classStatusName = ExtendHelper::buildEnumValueClassName(
            IssueStatusProvider::ENUM_CODE_STATUS
        );
        $classResolutionName = ExtendHelper::buildEnumValueClassName(
            IssueResolutionProvider::ENUM_CODE_RESOLUTION
        );

        $priorityMajor = $manager->getRepository($classPriorityName)
            ->find(IssuePriorityProvider::PRIORITY_MAJOR);
        $priorityBlocker = $manager->getRepository($classPriorityName)
            ->find(IssuePriorityProvider::PRIORITY_BLOCKER);
        $statusOpen = $manager->getRepository($classStatusName)
            ->find(IssueStatusProvider::STATUS_OPEN);
        $statusClose = $manager->getRepository($classStatusName)->find(IssueStatusProvider::STATUS_CLOSED);
        $resolutionDone = $manager->getRepository($classResolutionName)
            ->find(IssueResolutionProvider::RESOLUTION_DONE);
        $resolutionFailed = $manager->getRepository($classResolutionName)
            ->find(IssueResolutionProvider::RESOLUTION_FAILED);

        $this->setReference(self::ISSUE_PRIORITY_MAJOR, $priorityMajor);
        $this->setReference(self::ISSUE_PRIORITY_BLOCKER, $priorityBlocker);
        $this->setReference(self::ISSUE_STATUS_OPEN, $statusOpen);
        $this->setReference(self::ISSUE_STATUS_CLOSE, $statusClose);
        $this->setReference(self::ISSUE_RESOLUTION_DONE, $resolutionDone);
        $this->setReference(self::ISSUE_RESOLUTION_FAILED, $resolutionFailed);
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Tests\Functional\Entity\Repository;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\TestFrameworkBundle\Test\WebTestCase;

/**
 * Test Repository for Entity Issue
 */
class IssueRepositoryTest extends WebTestCase
{
    /**
     * @var IssueRepository
     */
    private $issueRepository;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->initClient();
        $this->loadFixtures(
            [
                '@OroIssueBundle/Tests/Functional/DataFixtures/issue_data.yml'
            ]
        );
        $registry = static::getContainer()->get('doctrine');
        $this->issueRepository = $registry->getRepository(Issue::class);
    }

    public function testGetCountStatusesWithNames(): void
    {
        $statuses = $this->issueRepository->getCountStatusesWithNames();
        self::assertEquals(
            $statuses,
            [
                ['statusName' => IssueStatusProvider::STATUS_FIELDS[IssueStatusProvider::STATUS_OPEN], 'value' => 3]
            ]
        );
    }
}

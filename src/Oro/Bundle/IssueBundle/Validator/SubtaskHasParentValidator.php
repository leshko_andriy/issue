<?php

namespace Oro\Bundle\IssueBundle\Validator;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validation constraint. Subtask must has parent issue
 */
class SubtaskHasParentValidator extends ConstraintValidator
{
    /**
     * @param Issue $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value instanceof Issue && $value->getType() === IssueTypeProvider::SUBTASK_TYPE && !$value->getParent()) {
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain('validators')
                ->atPath('parent')
                ->addViolation();
        }
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Validator\Constraints;

use Oro\Bundle\IssueBundle\Validator\SubtaskHasParentValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Constraint. Subtask must has parent issue
 */
class SubtaskHasParentConstraint extends Constraint
{
    /** @var string */
    public $message = 'oro.issue.validation.parent_message';

    /**
     * {@inheritdoc}
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return SubtaskHasParentValidator::class;
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Validator\Constraints;

use Oro\Bundle\IssueBundle\Validator\StorySubtaskValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Constraint. Dependency between issue fields: type, parent, child
 */
class StorySubtaskConstraint extends Constraint
{
    /** @var string */
    public $messageStory = 'oro.issue.validation.story_message';

    /** @var string */
    public $messageChild = 'oro.issue.validation.child_message';

    /** @var string */
    public $messageSubtask = 'oro.issue.validation.subtask_message';

    /**
     * {@inheritdoc}
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy(): string
    {
        return StorySubtaskValidator::class;
    }
}

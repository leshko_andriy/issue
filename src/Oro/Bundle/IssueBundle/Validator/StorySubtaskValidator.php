<?php

namespace Oro\Bundle\IssueBundle\Validator;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validation constraint. Dependency between issue fields: type, parent, child
 */
class StorySubtaskValidator extends ConstraintValidator
{
    /**
     * @param Issue $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if ($value->getChildren()) {
            foreach ($value->getChildren() as $child) {
                if ($child->getType() !== IssueTypeProvider::SUBTASK_TYPE) {
                    $this->context->buildViolation($constraint->messageChild)
                        ->setTranslationDomain('validators')
                        ->atPath('type')
                        ->addViolation();
                    break;
                }
            }
        }

        if ($value->getType() !== IssueTypeProvider::STORY_TYPE && count($value->getChildren())) {
            $this->context->buildViolation($constraint->messageStory)
                ->setTranslationDomain('validators')
                ->atPath('type')
                ->addViolation();
        }

        if ($value->getType() !== IssueTypeProvider::SUBTASK_TYPE && $value->getParent()) {
            $this->context->buildViolation($constraint->messageSubtask)
                ->setTranslationDomain('validators')
                ->atPath('type')
                ->addViolation();
        }
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Model;

use Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue;

/**
 * This class give ability extended our entity issue
 * @method setPriority(AbstractEnumValue $enum)
 * @method AbstractEnumValue getPriority()
 * @method setStatus(AbstractEnumValue $enum)
 * @method AbstractEnumValue getStatus()
 * @method setResolution(AbstractEnumValue $enum)
 * @method AbstractEnumValue getResolution()
 */
class ExtendIssue
{
    /**
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}

<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Oro\Bundle\EntityBundle\Tools\EntityRoutingHelper;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Oro\Bundle\FormBundle\Utils\FormUtils;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Listener set parent and type value and lock "parent" and "type" form field
 */
class FormSetParentEventListener
{
    public const ACTION_CREATE_ISSUE_SUBTASK = 'create_issue_subtask';

    /**
     * @var EntityRoutingHelper
     */
    private $entityRoutingHelper;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param EntityRoutingHelper $entityRoutingHelper
     * @param RequestStack $requestStack
     */
    public function __construct(EntityRoutingHelper $entityRoutingHelper, RequestStack $requestStack)
    {
        $this->entityRoutingHelper = $entityRoutingHelper;
        $this->requestStack = $requestStack;
    }

    /**
     * Lock "parent" and "type" field of the form and preset it when create issue subtask from the issue view page.
     *
     * @param FormProcessEvent $event
     */
    public function setParentAndLockForm(FormProcessEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return;
        }

        $action = $this->entityRoutingHelper->getAction($request);

        if ($action !== self::ACTION_CREATE_ISSUE_SUBTASK) {
            return;
        }

        $targetEntityClass = $this->entityRoutingHelper->getEntityClassName($request);

        if (!$targetEntityClass || !is_a($targetEntityClass, Issue::class, true)) {
            return;
        }

        $targetEntityId = $this->entityRoutingHelper->getEntityId($request);

        if (!$targetEntityId) {
            return;
        }

        $data = $event->getData();
        $form = $event->getForm();

        $data->setParent(
            $this->entityRoutingHelper->getEntity($targetEntityClass, $targetEntityId)
        );

        $data->setType(IssueTypeProvider::SUBTASK_TYPE);

        FormUtils::replaceFieldOptionsRecursive($form, 'parent', [
            'attr' => ['readonly' => true],
        ]);

        FormUtils::replaceFieldOptionsRecursive($form, 'type', [
            'attr' => ['readonly' => true],
        ]);
    }
}

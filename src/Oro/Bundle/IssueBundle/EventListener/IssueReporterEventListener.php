<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Set reporter for issue
 */
class IssueReporterEventListener
{
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Issue $issue
     * @param LifecycleEventArgs $args
     */
    public function prePersist(Issue $issue, LifecycleEventArgs $args): void
    {
        if ($issue->getReporter() === null) {
            $token = $this->tokenStorage->getToken();
            if ($token) {
                /** @var User $user */
                $user = $token->getUser();
                if ($user instanceof User) {
                    $issue->setReporter($user);
                }
            }
        }
    }
}

<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Modify fields: parent and relatedIssue
 * For parent: exclude current issue and exclude all types except Story
 * For related: exclude current issue, multiply and by_reference => false
 */
class IssueTypeSubscribe implements EventSubscriberInterface
{
    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'modifyFormField',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function modifyFormField(FormEvent $event): void
    {
        $form = $event->getForm();
        $issue = $event->getData();

        $parentOption = $form->get('parent')->getConfig()->getOptions();
        $options = [];
        if (isset($parentOption['attr'])) {
            $options['attr'] = $parentOption['attr'];
        }
        $options['query_builder'] = function (IssueRepository $repo) use ($issue) {
            return $repo->getStoryIssuesExcludeMe($issue);
        };
        $form->add('parent', $this->getTypeClass($form, 'parent'), $options);

        $form->add('relatedIssues', $this->getTypeClass($form, 'relatedIssues'), [
            'query_builder' => function (IssueRepository $repo) use ($issue) {
                return $repo->getIssuesExcludeMe($issue);
            },
            'multiple' => true,
            'by_reference' => false
        ]);
    }

    /**
     * @param FormInterface $form
     * @param string $field
     * @return string
     */
    private function getTypeClass(FormInterface $form, string $field): string
    {
        return get_class($form->get($field)->getConfig()->getType()->getInnerType());
    }
}

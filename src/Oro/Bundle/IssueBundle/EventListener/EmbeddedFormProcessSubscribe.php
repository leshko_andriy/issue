<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Oro\Bundle\EmbeddedFormBundle\Event\EmbeddedFormSubmitAfterEvent;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Auto add organization to embedded issue form
 */
class EmbeddedFormProcessSubscribe implements EventSubscriberInterface
{
    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            EmbeddedFormSubmitAfterEvent::EVENT_NAME => 'setOrganization',
        ];
    }

    /**
     * @param EmbeddedFormSubmitAfterEvent $event
     */
    public function setOrganization(EmbeddedFormSubmitAfterEvent $event): void
    {
        $entity = $event->getData();
        $entityEmbedded = $event->getFormEntity();

        if ($entity instanceof Issue) {
            $entity->setOrganization($entityEmbedded->getOwner());
        }
    }
}

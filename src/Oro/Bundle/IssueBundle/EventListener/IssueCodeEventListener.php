<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Oro\Bundle\IssueBundle\Entity\Issue;

/**
 * Set code for issue
 */
class IssueCodeEventListener
{
    /**
     * @param Issue $issue
     * @param LifecycleEventArgs $event
     */
    public function postPersist(Issue $issue, LifecycleEventArgs $event): void
    {
        if ($issue->getCode() === null) {
            $em = $event->getEntityManager();
            /**
             * When anybody upload this code via import.
             * Add unique identity
             */
            $uniqueCode = $this->generateCode($issue);
            $codeEntity = $em->getRepository(Issue::class)->findOneBy(['code' => $uniqueCode]);
            if ($codeEntity) {
                $uniqueCode .= microtime();
            }
            $event->getEntityManager()->getUnitOfWork()->scheduleExtraUpdate(
                $issue,
                ['code' => [null, $uniqueCode]]
            );
            $issue->setCode($uniqueCode);
        }
    }

    /**
     * @param Issue $issue
     * @return string
     */
    private function generateCode(Issue $issue): string
    {
        $prefix = '';
        $arrayWords = explode(' ', $issue->getSummary());
        foreach ($arrayWords as $word) {
            $prefix .= strtoupper($word[0]);
        }

        return $prefix . '-' . (string)$issue->getId();
    }
}

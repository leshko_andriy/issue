<?php

namespace Oro\Bundle\IssueBundle\EventListener;

use Oro\Bundle\EntityBundle\Tools\EntityRoutingHelper;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Oro\Bundle\FormBundle\Utils\FormUtils;
use Oro\Bundle\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Listener set assignee value and lock "assignee" form field
 */
class FormSetAssigneeEventListener
{
    public const ACTION_CREATE_ISSUE_MY = 'create_issue_my';

    /**
     * @var EntityRoutingHelper
     */
    private $entityRoutingHelper;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param EntityRoutingHelper $entityRoutingHelper
     * @param RequestStack $requestStack
     */
    public function __construct(EntityRoutingHelper $entityRoutingHelper, RequestStack $requestStack)
    {
        $this->entityRoutingHelper = $entityRoutingHelper;
        $this->requestStack = $requestStack;
    }

    /**
     * Lock "assignee" field of the form and preset it when create issue from the user view page.
     *
     * @param FormProcessEvent $event
     */
    public function setAssigneeAndLockForm(FormProcessEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return;
        }

        $action = $this->entityRoutingHelper->getAction($request);

        if ($action !== self::ACTION_CREATE_ISSUE_MY) {
            return;
        }

        $targetEntityClass = $this->entityRoutingHelper->getEntityClassName($request);

        if (!$targetEntityClass || !is_a($targetEntityClass, User::class, true)) {
            return;
        }

        $targetEntityId = $this->entityRoutingHelper->getEntityId($request);

        if (!$targetEntityId) {
            return;
        }

        $data = $event->getData();
        $form = $event->getForm();

        $data->setAssignee(
            $this->entityRoutingHelper->getEntity($targetEntityClass, $targetEntityId)
        );

        FormUtils::replaceFieldOptionsRecursive($form, 'assignee', [
            'attr' => ['readonly' => true],
        ]);
    }
}

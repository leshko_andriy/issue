<?php

namespace Oro\Bundle\IssueBundle\Controller;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for issue widgets
 * @Route("/widget")
 */
class IssueWidgetController extends Controller
{
    /**
     * Get page with date updated for issue
     * @Route("/updated-at/{id}", name="oro_issue_updated_at_widget")
     * @AclAncestor("oro_issue_view")
     * @param Issue $issue
     * @return Response
     */
    public function dateUpdatedAction(Issue $issue): Response
    {
        return $this->render('OroIssueBundle:IssueWidget:dateUpdated.html.twig', ['entity' => $issue]);
    }
}

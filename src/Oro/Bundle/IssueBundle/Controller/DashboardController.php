<?php

namespace Oro\Bundle\IssueBundle\Controller;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that displays bar chart widget on dashboard page
 * @Route("/dashboard")
 */
class DashboardController extends Controller
{
    /**
     * Show page with bar chart status diagram
     * @Route("/{widget}", name="oro_issue_dashboard_bar_chart_status_issues")
     * @AclAncestor("oro_issue_view")
     * @param string $widget
     * @return Response
     */
    public function barChartStatusIssuesAction(string $widget): Response
    {
        $data = $this->getDoctrine()->getRepository(Issue::class)->getCountStatusesWithNames();
        $widgetAttr = $this->get('oro_dashboard.widget_configs')->getWidgetAttributesForTwig($widget);
        $widgetAttr['chartView'] = $this->get('oro_chart.view_builder')
            ->setArrayData($data)
            ->setOptions(
                [
                    'name' => 'bar_chart',
                    'data_schema' => [
                        'label' => ['field_name' => 'statusName'],
                        'value' => ['field_name' => 'value'],
                    ]
                ]
            )
            ->getView();

        return $this->render('OroIssueBundle:Dashboard:barChartStatusIssues.html.twig', $widgetAttr);
    }
}

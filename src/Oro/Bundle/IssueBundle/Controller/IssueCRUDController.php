<?php

namespace Oro\Bundle\IssueBundle\Controller;

use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Form\Type\IssueType;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for create, update, view issues
 */
class IssueCRUDController extends Controller
{
    /**
     * Show grid with all issues
     * @Route("/", name="oro_issue_index")
     * @Acl(
     *      id="oro_issue_view",
     *      type="entity",
     *      class="OroIssueBundle:Issue",
     *      permission="VIEW"
     * )
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('OroIssueBundle:IssueCRUD:index.html.twig', ['gridName' => 'issue-list-grid']);
    }

    /**
     * Show view page of specific issue
     * @Route("/view/{id}", name="oro_issue_view", requirements={"id"="\d+"})
     * @AclAncestor("oro_issue_view")
     * @param Issue $issue
     * @return Response
     */
    public function viewAction(Issue $issue): Response
    {
        return $this->render('OroIssueBundle:IssueCRUD:view.html.twig', ['entity' => $issue]);
    }

    /**
     * Show create page for create issue
     * @Route("/create", name="oro_issue_create")
     * @Acl(
     *      id="oro_issue_create",
     *      type="entity",
     *      class="OroIssueBundle:Issue",
     *      permission="CREATE"
     * )
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $issue = new Issue();
        $createData = $this->createUpdate($request, $issue);
        if ($createData instanceof Response) {
            return $createData;
        }

        return $this->render('OroIssueBundle:IssueCRUD:update.html.twig', $createData);
    }

    /**
     * Show update page for updateissue
     * @Route("/update/{id}", name="oro_issue_update", requirements={"id"="\d+"})
     * @Acl(
     *      id="oro_issue_update",
     *      type="entity",
     *      class="OroIssueBundle:Issue",
     *      permission="EDIT"
     * )
     * @param Request $request
     * @param Issue $issue
     * @return Response
     */
    public function updateAction(Request $request, Issue $issue): Response
    {
        $updateData = $this->createUpdate($request, $issue);
        if ($updateData instanceof Response) {
            return $updateData;
        }

        return $this->render('OroIssueBundle:IssueCRUD:update.html.twig', $updateData);
    }

    /**
     * @param Request $request
     * @param Issue $issue
     *
     * @return Response|array
     */
    protected function createUpdate(Request $request, Issue $issue)
    {
        $updateResult = $this->get('oro_form.update_handler')->update(
            $issue,
            $this->createForm(IssueType::class, $issue),
            $this->get('translator')->trans('oro.issue.saved_message'),
            $request,
            null,
            'oro_issue_update'
        );

        return $updateResult;
    }
}

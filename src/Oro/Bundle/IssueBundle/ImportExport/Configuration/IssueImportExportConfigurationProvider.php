<?php

namespace Oro\Bundle\IssueBundle\ImportExport\Configuration;

use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfiguration;
use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfigurationInterface;
use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfigurationProviderInterface;
use Oro\Bundle\IssueBundle\Entity\Issue;

/**
 * Config for Import export issue
 */
class IssueImportExportConfigurationProvider implements ImportExportConfigurationProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function get(): ImportExportConfigurationInterface
    {
        return new ImportExportConfiguration([
            ImportExportConfiguration::FIELD_ENTITY_CLASS => Issue::class,
            ImportExportConfiguration::FIELD_EXPORT_TEMPLATE_PROCESSOR_ALIAS => 'oro_issue',
            ImportExportConfiguration::FIELD_EXPORT_PROCESSOR_ALIAS => 'oro_issue',
            ImportExportConfiguration::FIELD_IMPORT_PROCESSOR_ALIAS => 'oro_issue',
        ]);
    }
}

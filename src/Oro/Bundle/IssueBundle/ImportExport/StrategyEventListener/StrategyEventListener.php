<?php

namespace Oro\Bundle\IssueBundle\ImportExport\StrategyEventListener;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ImportExportBundle\Event\StrategyEvent;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event before import issue
 */
class StrategyEventListener implements EventSubscriberInterface
{
    /**
     * @var DoctrineHelper
     */
    protected $doctrineHelper;

    /**
     * @param DoctrineHelper $doctrineHelper
     */
    public function __construct(DoctrineHelper $doctrineHelper)
    {
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            StrategyEvent::PROCESS_BEFORE => 'beforeImportStrategy',
        ];
    }

    /**
     * @param StrategyEvent $event
     * @return void
     */
    public function beforeImportStrategy(StrategyEvent $event): void
    {
        $em = $this->doctrineHelper->getEntityManager(Issue::class);
        /** @var Issue $entity */
        $entity = $event->getEntity();
        if (!$entity instanceof Issue) {
            return;
        }

        /** @var Issue[] $children */
        $children = $entity->getChildren();
        $repo = $em->getRepository(Issue::class);
        if (count($children)) {
            $childObjects = $repo->findBy(['code' => $entity->getChildCodes()]);
            if (count($childObjects) !== count($entity->getChildren())) {
                $event->getContext()->addPostponedRow(
                    $event->getContext()->getValue('rawItemData')
                );
                $event->setEntity(null);
            }
        }
    }
}

<?php

namespace Oro\Bundle\IssueBundle\ImportExport\TemplateFixture;

use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\ImportExportBundle\TemplateFixture\AbstractTemplateRepository;
use Oro\Bundle\ImportExportBundle\TemplateFixture\TemplateFixtureInterface;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\UserBundle\Entity\User;

/**
 * Template fixture for import issue
 */
class IssueFixture extends AbstractTemplateRepository implements TemplateFixtureInterface
{
    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return Issue::class;
    }

    /**
     * @return \Iterator
     */
    public function getData(): \Iterator
    {
        return $this->getEntityData('Example Issue');
    }

    /**
     * @param string $key
     * @param Issue $entity
     */
    public function fillEntityData($key, $entity): void
    {
        $classPriorityName = ExtendHelper::buildEnumValueClassName(IssuePriorityProvider::ENUM_CODE_PRIORITY);
        $classResolutionName = ExtendHelper::buildEnumValueClassName(IssueResolutionProvider::ENUM_CODE_RESOLUTION);
        $user = new User();
        $user->setUsername('admin');
        $entity->setCode('T-12');
        $entity->setSummary('test Summary');
        $entity->setType(IssueTypeProvider::STORY_TYPE);
        $entity->setDescription('test description');
        $entity->setAssignee($user);
        $entity->setReporter($user);
        $resolution = new $classResolutionName(
            IssueResolutionProvider::RESOLUTION_DONE,
            IssueResolutionProvider::RESOLUTION_FIELDS[IssueResolutionProvider::RESOLUTION_DONE]
        );
        $priority = new $classPriorityName(
            IssuePriorityProvider::PRIORITY_BLOCKER,
            IssuePriorityProvider::PRIORITY_FIELDS[IssuePriorityProvider::PRIORITY_BLOCKER]
        );

        $entity->setPriority($priority);
        $entity->setResolution($resolution);

        $issueChild = new Issue();
        $issueChild->setCode('T-2');

        $entity->addChild($issueChild);
    }

    /**
     * @param string $key
     * @return Issue
     */
    protected function createEntity($key): Issue
    {
        return new Issue();
    }
}

<?php

namespace Oro\Bundle\IssueBundle\ImportExport\Strategy;

use Oro\Bundle\ImportExportBundle\Strategy\Import\ConfigurableAddOrReplaceStrategy;
use Oro\Bundle\IssueBundle\Entity\Issue;

/**
 * Strategy for import issue
 */
class AddOrReplaceStrategy extends ConfigurableAddOrReplaceStrategy
{
    /**
     * @var string
     */
    private $validationGroup;

    /**
     * @param string $validationGroup
     * @return void
     */
    public function setValidationGroup(string $validationGroup): void
    {
        $this->validationGroup = $validationGroup;
    }

    /**
     * @return string
     */
    public function getValidationGroup(): string
    {
        return $this->validationGroup;
    }

    /**
     * @param Issue $entity
     * @return Issue|null
     */
    protected function validateAndUpdateContext($entity): ?Issue
    {
        // validate entity
        $validationErrors = $this->strategyHelper->validateEntity($entity, null, $this->getValidationGroup());
        if ($validationErrors) {
            $this->processValidationErrors($entity, $validationErrors);

            return null;
        }

        $this->updateContextCounters($entity);

        return $entity;
    }
}

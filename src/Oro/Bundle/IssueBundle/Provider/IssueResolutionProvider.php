<?php

namespace Oro\Bundle\IssueBundle\Provider;

/**
 * Resolutions provider
 */
class IssueResolutionProvider
{
    public const RESOLUTION_DONE = 'done';
    public const RESOLUTION_FIXED = 'fixed';
    public const RESOLUTION_DUPLICATE = 'duplicate';
    public const RESOLUTION_INCOMPLETE = 'incomplete';
    public const RESOLUTION_WONTFIX = 'wontfix';
    public const RESOLUTION_DECLINED = 'declined';
    public const RESOLUTION_FAILED = 'failed';

    public const RESOLUTION_FIELDS = [
        self::RESOLUTION_DONE => 'Done',
        self::RESOLUTION_FIXED=> 'Fixed',
        self::RESOLUTION_DUPLICATE => 'Duplicate',
        self::RESOLUTION_INCOMPLETE => 'Incomplete',
        self::RESOLUTION_WONTFIX => 'WontFix',
        self::RESOLUTION_FAILED => 'Failed',
        self::RESOLUTION_DECLINED => 'Declined'
    ];

    public const ENUM_CODE_RESOLUTION = 'issue_resolution';
}

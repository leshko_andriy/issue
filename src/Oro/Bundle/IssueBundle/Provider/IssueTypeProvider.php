<?php

namespace Oro\Bundle\IssueBundle\Provider;

/**
 * Type provider
 */
class IssueTypeProvider
{
    public const SUBTASK_TYPE = 'SubTask';
    public const BUG_TYPE = 'Bug';
    public const STORY_TYPE = 'Story';
    public const TASK_TYPE = 'Task';

    public const TYPES = [
        self::BUG_TYPE => self::BUG_TYPE,
        self::SUBTASK_TYPE => self::SUBTASK_TYPE,
        self::STORY_TYPE => self::STORY_TYPE,
        self::TASK_TYPE => self::TASK_TYPE
    ];
}

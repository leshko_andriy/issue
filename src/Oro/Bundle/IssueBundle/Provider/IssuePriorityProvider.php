<?php

namespace Oro\Bundle\IssueBundle\Provider;

/**
 * Priority provider
 */
class IssuePriorityProvider
{
    public const PRIORITY_BLOCKER = 'blocker';
    public const PRIORITY_CRITICAL = 'critical';
    public const PRIORITY_MAJOR = 'major';
    public const PRIORITY_TRIVIAL = 'trivial';
    public const PRIORITY_MINOR = 'minor';

    public const PRIORITY_FIELDS = [
        self::PRIORITY_BLOCKER => 'Blocker',
        self::PRIORITY_CRITICAL => 'Critical',
        self::PRIORITY_MAJOR => 'Major',
        self::PRIORITY_TRIVIAL => 'Trivial',
        self::PRIORITY_MINOR => 'Minor',
    ];

    public const ENUM_CODE_PRIORITY = 'issue_priority';
}

<?php

namespace Oro\Bundle\IssueBundle\Provider;

/**
 * Statuses provider
 */
class IssueStatusProvider
{
    public const STATUS_OPEN = 'open';
    public const STATUS_INPROGRESS = 'in_progress';
    public const STATUS_RESOLVED = 'resolved';
    public const STATUS_REOPENED = 'reopened';
    public const STATUS_CLOSED = 'closed';

    public const STATUS_FIELDS = [
        self::STATUS_OPEN => 'Open',
        self::STATUS_INPROGRESS => 'In progress',
        self::STATUS_RESOLVED => 'Resolved',
        self::STATUS_REOPENED => 'Reopen',
        self::STATUS_CLOSED => 'Closed'
    ];

    public const ENUM_CODE_STATUS = 'issue_status';
}

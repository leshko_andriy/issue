<?php

namespace Oro\Bundle\IssueBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\OrganizationBundle\Entity\BusinessUnit;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\OrganizationBundle\Migrations\Data\ORM\LoadOrganizationAndBusinessUnitData;
use Oro\Bundle\ReportBundle\Entity\Report;
use Oro\Bundle\ReportBundle\Entity\ReportType;
use Oro\Bundle\ReportBundle\Migrations\Data\ORM\LoadReportTypes;

/**
 * Load report for issue
 */
class LoadReportingData extends AbstractFixture implements DependentFixtureInterface
{
    /** @var array  */
    private const REPORT_DEFINITION = [
        'columns' => [
            [
                'name' => 'type',
                'label' => 'Type',
                'func' => '',
                'sorting' => '',
            ],
            [
                'name' => 'type',
                'label' => 'Count',
                'func' =>
                    [
                        'name' => 'Count',
                        'group_type' => 'aggregates',
                        'group_name' => 'string',
                        'return_type' => 'integer',
                    ],
                'sorting' => 'DESC',
            ],
        ],
        'grouping_columns' => [
            ['name' => 'type']
        ],
        'filters' => []
    ];
    private const REPORT_NAME = 'Issue by type';
    private const REPORT_DESCRIPTION = 'Get count issues by type';

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadOrganizationAndBusinessUnitData::class,
            LoadReportTypes::class
        ];
    }

    /**
     * Load report
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        /** @var BusinessUnit $owner */
        $owner = $manager->getRepository(BusinessUnit::class)->getFirst();

        /** @var Organization $organization */
        $organization = $manager->getRepository(Organization::class)->getFirst();

        /** @var ReportType $type */
        $type = $manager->getRepository(ReportType::class)->findOneBy(['name' => ReportType::TYPE_TABLE]);
        $report = new Report();
        $report->setName(self::REPORT_NAME);
        $report->setDescription(self::REPORT_DESCRIPTION);
        $report->setEntity(Issue::class);
        $report->setType($type);
        $report->setDefinition(json_encode(self::REPORT_DEFINITION));
        $report->setOwner($owner);
        $report->setOrganization($organization);
        $manager->persist($report);
        $manager->flush();
    }
}

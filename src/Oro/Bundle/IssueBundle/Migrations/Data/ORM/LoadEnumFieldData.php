<?php

namespace Oro\Bundle\IssueBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Oro\Bundle\EntityExtendBundle\Entity\Repository\EnumValueRepository;
use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;

/**
 * Load enum field for issue
 */
class LoadEnumFieldData extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $priorityClassName = ExtendHelper::buildEnumValueClassName(IssuePriorityProvider::ENUM_CODE_PRIORITY);
        $statusClassName = ExtendHelper::buildEnumValueClassName(IssueStatusProvider::ENUM_CODE_STATUS);
        $resolutionClassName = ExtendHelper::buildEnumValueClassName(IssueResolutionProvider::ENUM_CODE_RESOLUTION);

        $this->addValues(
            $manager,
            $priorityClassName,
            IssuePriorityProvider::PRIORITY_FIELDS
        );
        $this->addValues(
            $manager,
            $statusClassName,
            IssueStatusProvider::STATUS_FIELDS
        );
        $this->addValues(
            $manager,
            $resolutionClassName,
            IssueResolutionProvider::RESOLUTION_FIELDS,
            false
        );
    }

    /**
     * @param ObjectManager $manager
     * @param string $className
     * @param $fields
     * @param bool $defaultPresent
     */
    private function addValues(
        ObjectManager $manager,
        string $className,
        $fields,
        $defaultPresent = true
    ): void {
        /** @var EnumValueRepository $repo */
        $repo = $manager->getRepository($className);
        $p = 1;
        foreach ($fields as $key => $name) {
            $isDefault = ($defaultPresent && $p === 1) ? true : false;
            $enumOption = $repo->createEnumValue($name, $p++, $isDefault, $key);
            $manager->persist($enumOption);
        }
        $manager->flush();
    }
}

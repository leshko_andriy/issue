<?php

namespace Oro\Bundle\IssueBundle\Migrations\Data\Demo\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\UserBundle\DataFixtures\UserUtilityTrait;

/**
 * Loading demo data for Issue entity
 */
class LoadIssueData extends AbstractFixture
{
    use UserUtilityTrait;

    /**
     * Load issues
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $assignee = $this->getFirstUser($manager);

        $priorityClassName = ExtendHelper::buildEnumValueClassName(IssuePriorityProvider::ENUM_CODE_PRIORITY);
        $statusClassName = ExtendHelper::buildEnumValueClassName(IssueStatusProvider::ENUM_CODE_STATUS);

        $issuesData = $this->getData();

        $story = null;
        foreach ($issuesData as $k => $issueData) {
            $issue = new Issue();
            $issue->setSummary($issueData['summary']);
            $issue->setDescription($issueData['description']);
            $issue->setAssignee($assignee);
            $issue->setReporter($assignee);
            $issue->setOrganization($assignee->getOrganization());
            $issue->setType($issueData['type']);
            $issue->setPriority($manager->getRepository($priorityClassName)->find($issueData['priority']));
            $issue->setStatus($manager->getRepository($statusClassName)->find(IssueStatusProvider::STATUS_OPEN));

            if ($issue->getType() === IssueTypeProvider::STORY_TYPE) {
                $story = $issue;
            }

            if ($issue->getType() === IssueTypeProvider::SUBTASK_TYPE && $story instanceof Issue) {
                $issue->setParent($story);
            }

            if ($story instanceof Issue && in_array($k, [2,5,9])) {
                $issue->addRelatedIssue($story);
            }

            $manager->persist($issue);
        }
        $manager->flush();
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        return [
            [
                'summary' => 'summary simple 1',
                'description' => 'Description simple 1',
                'type' => IssueTypeProvider::STORY_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 2',
                'description' => 'Description simple 2',
                'type' => IssueTypeProvider::SUBTASK_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 3',
                'description' => 'Description simple 3',
                'type' => IssueTypeProvider::BUG_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_BLOCKER
            ],
            [
                'summary' => 'summary simple 4',
                'description' => 'Description simple 4',
                'type' => IssueTypeProvider::TASK_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 5',
                'description' => 'Description simple 5',
                'type' => IssueTypeProvider::TASK_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_BLOCKER
            ],
            [
                'summary' => 'summary simple 6',
                'description' => 'Description simple 6',
                'type' => IssueTypeProvider::BUG_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 7',
                'description' => 'Description simple 7',
                'type' => IssueTypeProvider::BUG_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 8',
                'description' => 'Description simple 8',
                'type' => IssueTypeProvider::STORY_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_MAJOR
            ],
            [
                'summary' => 'summary simple 9',
                'description' => 'Description simple 9',
                'type' => IssueTypeProvider::TASK_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_BLOCKER
            ],
            [
                'summary' => 'summary simple 10',
                'description' => 'Description simple 10',
                'type' => IssueTypeProvider::BUG_TYPE,
                'priority' => IssuePriorityProvider::PRIORITY_BLOCKER
            ]
        ];
    }
}

<?php

namespace Oro\Bundle\IssueBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtension;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\Migration\OroOptions;
use Oro\Bundle\IssueBundle\Migrations\Schema\OroIssueBundleInstaller;
use Oro\Bundle\IssueBundle\Provider\IssuePriorityProvider;
use Oro\Bundle\IssueBundle\Provider\IssueResolutionProvider;
use Oro\Bundle\IssueBundle\Provider\IssueStatusProvider;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * Migration need data for issue bundle
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class OroIssueBundle implements
    Migration,
    ActivityExtensionAwareInterface,
    ExtendExtensionAwareInterface
{
    private const ISSUE_TABLE_NAME = 'oro_issues';
    private const ORO_EMAIL_TABLE_NAME = 'oro_email';
    private const ORO_NOTE_TABLE_NAME = 'oro_note';
    private const ORO_USER_TABLE_NAME = 'oro_user';
    private const ORO_ORGANIZATION_TABLE_NAME = 'oro_organization';
    private const MANY_TO_MANY_ISSUES_RELATED_TABLE_NAME = 'oro_issues_related';
    private const MANY_TO_MANY_ISSUES_COLLABORATORS_TABLE_NAME = 'oro_issue_collaborators';

    /**
     * @var ExtendExtension
     */
    protected $extendExtension;

    /**
     * @var ActivityExtension
     */
    protected $activityExtension;

    /**
     * @inheritdoc
     */
    public function setActivityExtension(ActivityExtension $activityExtension): void
    {
        $this->activityExtension = $activityExtension;
    }

    /**
     * @inheritdoc
     */
    public function setExtendExtension(ExtendExtension $extendExtension): void
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries): void
    {
        /** Tables generation **/
        $this->createOroIssuesTable($schema);
        $this->createOroIssuesRelatedTable($schema);
        $this->createOroIssueCollaboratorsTable($schema);

        /** Foreign keys generation **/
        $this->addOroIssuesForeignKeys($schema);
        $this->addOroIssuesRelatedForeignKeys($schema);
        $this->addOroIssueCollaboratorsForeignKeys($schema);

        $this->addEnumField($schema);
        $this->addActivity($schema);
    }

    /**
     * Create oro_issues table
     *
     * @param Schema $schema
     */
    private function createOroIssuesTable(Schema $schema): void
    {
        $table = $schema->createTable(OroIssueBundleInstaller::ISSUE_TABLE_NAME);
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('reporter_id', 'integer', ['notnull' => false]);
        $table->addColumn('assignee_id', 'integer', []);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('parent_id', 'integer', ['notnull' => false]);
        $table->addColumn('summary', 'string', ['length' => 255]);
        $table->addColumn('code', 'string', ['length' => 255, 'notnull' => false]);
        $table->addColumn('description', 'text', []);
        $table->addColumn('type', 'string', ['length' => 64]);
        $table->addColumn('created_at', 'datetime', ['comment' => '(DC2Type:datetime)']);
        $table->addColumn(
            'updated_at',
            'datetime',
            ['comment' => '(DC2Type:datetime)']
        );
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['code'], 'uniq_aada29b177153098');
    }

    /**
     * Create oro_issues_related table
     *
     * @param Schema $schema
     */
    private function createOroIssuesRelatedTable(Schema $schema): void
    {
        $table = $schema->createTable(OroIssueBundleInstaller::MANY_TO_MANY_ISSUES_RELATED_TABLE_NAME);
        $table->addColumn('issue_id', 'integer', []);
        $table->addColumn('issue_related_id', 'integer', []);
        $table->setPrimaryKey(['issue_id', 'issue_related_id']);
    }

    /**
     * Create oro_issue_collaborators table
     *
     * @param Schema $schema
     */
    private function createOroIssueCollaboratorsTable(Schema $schema): void
    {
        $table = $schema->createTable(OroIssueBundleInstaller::MANY_TO_MANY_ISSUES_COLLABORATORS_TABLE_NAME);
        $table->addColumn('issue_id', 'integer', []);
        $table->addColumn('user_id', 'integer', []);
        $table->setPrimaryKey(['issue_id', 'user_id']);
    }

    /**
     * Add oro_issues foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    private function addOroIssuesForeignKeys(Schema $schema): void
    {
        $table = $schema->getTable(self::ISSUE_TABLE_NAME);
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ISSUE_TABLE_NAME),
            ['parent_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ORO_ORGANIZATION_TABLE_NAME),
            ['organization_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ORO_USER_TABLE_NAME),
            ['assignee_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ORO_USER_TABLE_NAME),
            ['reporter_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
    }

    /**
     * Add oro_issues_related foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    private function addOroIssuesRelatedForeignKeys(Schema $schema): void
    {
        $table = $schema->getTable(self::MANY_TO_MANY_ISSUES_RELATED_TABLE_NAME);
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ISSUE_TABLE_NAME),
            ['issue_related_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ISSUE_TABLE_NAME),
            ['issue_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
    }

    /**
     * Add oro_issue_collaborators foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    private function addOroIssueCollaboratorsForeignKeys(Schema $schema): void
    {
        $table = $schema->getTable(self::MANY_TO_MANY_ISSUES_COLLABORATORS_TABLE_NAME);
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ORO_USER_TABLE_NAME),
            ['user_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable(self::ISSUE_TABLE_NAME),
            ['issue_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
    }

    /**
     * add enum field
     * @param Schema $schema
     */
    private function addEnumField(Schema $schema): void
    {
        /**
         * status field. this field will be associate with workflow
         */
        $this
            ->extendExtension
            ->addEnumField(
                $schema,
                self::ISSUE_TABLE_NAME,
                'status',
                IssueStatusProvider::ENUM_CODE_STATUS,
                false,
                false,
                [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'importexport' => [
                        'excluded' => true
                    ],
                ]
            )
            ->addOption(
                OroOptions::KEY,
                $this->getOptionEnumField(IssueStatusProvider::STATUS_FIELDS)
            );

        /**
         * resolution field.
         */
        $this
            ->extendExtension
            ->addEnumField(
                $schema,
                self::ISSUE_TABLE_NAME,
                'resolution',
                IssueResolutionProvider::ENUM_CODE_RESOLUTION,
                false,
                false,
                [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM]
                ]
            )
            ->addOption(
                OroOptions::KEY,
                $this->getOptionEnumField(IssueResolutionProvider::RESOLUTION_FIELDS)
            );

        /**
         * priority field.
         */
        $this->
            extendExtension->
            addEnumField(
                $schema,
                self::ISSUE_TABLE_NAME,
                'priority',
                IssuePriorityProvider::ENUM_CODE_PRIORITY,
                false,
                false,
                [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM]
                ]
            )
            ->addOption(
                OroOptions::KEY,
                $this->getOptionEnumField(IssuePriorityProvider::PRIORITY_FIELDS)
            );
    }

    /**
     * @param array $values
     * @return OroOptions
     */
    private function getOptionEnumField(array $values): OroOptions
    {
        $options = new OroOptions();
        $options->set(
            'enum',
            'immutable_codes',
            array_keys($values)
        );

        return $options;
    }

    /**
     * add activity functional table (email, note)
     * @param Schema $schema
     */
    private function addActivity(Schema $schema): void
    {
        /**
         * email activity
         */
        $this->activityExtension->addActivityAssociation(
            $schema,
            OroIssueBundleInstaller::ORO_NOTE_TABLE_NAME,
            OroIssueBundleInstaller::ISSUE_TABLE_NAME
        );

        /**
         * note activity
         */
        $this->activityExtension->addActivityAssociation(
            $schema,
            OroIssueBundleInstaller::ORO_EMAIL_TABLE_NAME,
            OroIssueBundleInstaller::ISSUE_TABLE_NAME,
            true
        );
    }
}

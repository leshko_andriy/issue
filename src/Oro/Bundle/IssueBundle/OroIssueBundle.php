<?php

namespace Oro\Bundle\IssueBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle for academic project "Bug tracking"
 */
class OroIssueBundle extends Bundle
{
}

<?php

namespace Oro\Bundle\IssueBundle\Form\Type;

use Oro\Bundle\FormBundle\Form\Type\Select2EntityType;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * type issue for form entity field which has relation to issue
 */
class IssueEntityType extends Select2EntityType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'required' => false,
                'class' => Issue::class,
                'choice_label' => 'code'
            ]
        );
        parent::configureOptions($resolver);
    }
}

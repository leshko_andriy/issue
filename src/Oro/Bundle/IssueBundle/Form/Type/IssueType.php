<?php

namespace Oro\Bundle\IssueBundle\Form\Type;

use Oro\Bundle\EntityExtendBundle\Form\Type\EnumChoiceType;
use Oro\Bundle\FormBundle\Form\Type\Select2ChoiceType;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\EventListener\IssueTypeSubscribe;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class for form issue
 */
class IssueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('summary', TextType::class, [
                'required' => true,
                'label' => 'oro.issue.summary.label'
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'oro.issue.description.label'
            ])
            ->add('priority', EnumChoiceType::class, [
                'required' => true,
                'label' => 'oro.issue.priority.label',
                'enum_code' => 'issue_priority',
            ])
            ->add('type', Select2ChoiceType::class, [
                'required' => true,
                'label' => 'oro.issue.type.label',
                'choices' => IssueTypeProvider::TYPES,
            ])
            ->add('assignee', IssueUserType::class)
            ->add('parent', IssueEntityType::class)
            ->add('relatedIssues', IssueEntityType::class)
            ->addEventSubscriber(new IssueTypeSubscribe())
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Issue::class,
            'validation_groups' => 'UI',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'oro_issue';
    }
}

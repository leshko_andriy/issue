<?php

namespace Oro\Bundle\IssueBundle\Form\Type;

use Oro\Bundle\FormBundle\Form\Type\Select2EntityType;
use Oro\Bundle\UserBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * type for issue entity which has relation to user entity
 */
class IssueUserType extends Select2EntityType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'required' => false,
                'class' => User::class,
                'choice_label' => 'fullName'
            ]
        );
        parent::configureOptions($resolver);
    }
}

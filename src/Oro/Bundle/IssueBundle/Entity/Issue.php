<?php

namespace Oro\Bundle\IssueBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityBundle\EntityProperty\DatesAwareInterface;
use Oro\Bundle\EntityBundle\EntityProperty\DatesAwareTrait;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\IssueBundle\Model\ExtendIssue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\UserBundle\Entity\User;

/**
 * Entity which describe all field and relation for oro_issues table
 * @ORM\Table(name="oro_issues")
 * @ORM\Entity(repositoryClass="Oro\Bundle\IssueBundle\Entity\Repository\IssueRepository")
 * @Config(
 *      routeName="oro_issue_index",
 *      routeView="oro_issue_view",
 *      routeUpdate="oro_issue_update",
 *      defaultValues={
 *          "entity"={
 *              "icon"="fa-bug"
 *          },
 *          "ownership"={
 *              "owner_type"="USER",
 *              "owner_field_name"="assignee",
 *              "owner_column_name"="assignee_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "workflow"={
 *              "show_step_in_grid"=false
 *          },
 *          "security"={
 *              "type"="ACL",
 *          },
 *          "dataaudit"={
 *              "auditable"=true
 *          },
 *          "tag"={
 *              "enabled"=true,
 *              "enableGridColumn"=false,
 *              "enableGridFilter"=false
 *          }
 *      }
 * )
 */
class Issue extends ExtendIssue implements DatesAwareInterface
{
    use DatesAwareTrait;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="string", length=255, nullable=false)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true, unique=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "identity"=true
     *          },
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=64, nullable=false)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $type;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="reporter_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $reporter;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="assignee_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $assignee;

    /**
     * @var Organization|null
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $organization;

    /**
     * @var Collection|Issue[]
     *
     * @ORM\ManyToMany(targetEntity="Issue", mappedBy="relatedIssues")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $relatedWith;

    /**
     * @var Collection|Issue[]
     *
     * @ORM\ManyToMany(targetEntity="Issue", inversedBy="relatedWith")
     * @ORM\JoinTable(name="oro_issues_related",
     *      joinColumns={@ORM\JoinColumn(name="issue_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="issue_related_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $relatedIssues;

    /**
     * @var Collection|User[]
     *
     * @ORM\ManyToMany(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinTable(name="oro_issue_collaborators",
     *      joinColumns={@ORM\JoinColumn(name="issue_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $collaborators;

    /**
     * @var Issue|null
     *
     * @ORM\ManyToOne(targetEntity="Issue", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $parent;

    /**
     * @var Collection|Issue[]
     *
     * @ORM\OneToMany(targetEntity="Issue", mappedBy="parent")
     */
    protected $children;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     * @ConfigField(
     *      defaultValues={
     *          "entity"={
     *              "label"="oro.ui.created_at"
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at")
     * @ConfigField(
     *      defaultValues={
     *          "entity"={
     *              "label"="oro.ui.updated_at"
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->collaborators = new ArrayCollection();
        $this->relatedIssues = new ArrayCollection();
        $this->relatedWith = new ArrayCollection();
        parent::__construct();
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set summary.
     *
     * @param string $summary
     *
     * @return Issue
     */
    public function setSummary(string $summary): Issue
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary.
     *
     * @return string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Issue
     */
    public function setCode(string $code): Issue
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Issue
     */
    public function setDescription(string $description): Issue
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Issue
     */
    public function setType(string $type): Issue
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set reporter.
     *
     * @param User|null $reporter
     *
     * @return Issue
     */
    public function setReporter(?User $reporter): Issue
    {
        $this->reporter = $reporter;
        if ($reporter instanceof User) {
            $this->addCollaborator($reporter);
        }

        return $this;
    }

    /**
     * Get reporter.
     *
     * @return User|null
     */
    public function getReporter(): ?User
    {
        return $this->reporter;
    }

    /**
     * Set assignee.
     *
     * @param User|null $assignee
     *
     * @return Issue
     */
    public function setAssignee(?User $assignee): Issue
    {
        $this->assignee = $assignee;
        if ($assignee instanceof User) {
            $this->addCollaborator($assignee);
        }

        return $this;
    }

    /**
     * Get assignee.
     *
     * @return User|null
     */
    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    /**
     * Set organization.
     *
     * @param Organization|null $organization
     *
     * @return Issue
     */
    public function setOrganization(?Organization $organization): Issue
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization.
     *
     * @return Organization|null
     */
    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    /**
     * Add relatedIssue.
     *
     * @param Issue $relatedIssue
     *
     * @return Issue
     */
    public function addRelatedIssue(Issue $relatedIssue): Issue
    {
        if (!$this->getRelatedIssues()->contains($relatedIssue)) {
            $this->relatedIssues[] = $relatedIssue;
        }

        if (!$relatedIssue->getRelatedIssues()->contains($this)) {
            $relatedIssue->addRelatedIssue($this);
        }

        return $this;
    }

    /**
     * Remove relatedIssue.
     *
     * @param Issue $relatedIssue
     *
     * @return Issue
     */
    public function removeRelatedIssue(Issue $relatedIssue): Issue
    {
        if ($this->getRelatedIssues()->contains($relatedIssue)) {
            $this->relatedIssues->removeElement($relatedIssue);
        }

        if ($relatedIssue->getRelatedIssues()->contains($this)) {
            $relatedIssue->removeRelatedIssue($this);
        }

        return $this;
    }

    /**
     * Get relatedIssues.
     *
     * @return Collection|Issue[]
     */
    public function getRelatedIssues(): Collection
    {
        return $this->relatedIssues;
    }

    /**
     * Add collaborator.
     *
     * @param User $collaborator
     *
     * @return Issue
     */
    public function addCollaborator(User $collaborator): Issue
    {
        if (!$this->getCollaborators()->contains($collaborator)) {
            $this->collaborators[] = $collaborator;
        }

        return $this;
    }

    /**
     * Remove collaborator.
     *
     * @param User $collaborator
     *
     * @return Issue
     */
    public function removeCollaborator(User $collaborator): Issue
    {
        $this->collaborators->removeElement($collaborator);

        return $this;
    }

    /**
     * Get collaborators.
     *
     * @return Collection|User[]
     */
    public function getCollaborators(): Collection
    {
        return $this->collaborators;
    }

    /**
     * Set parent.
     *
     * @param Issue|null $parent
     *
     * @return Issue
     */
    public function setParent(?Issue $parent): Issue
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return Issue|null
     */
    public function getParent(): ?Issue
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param Issue $child
     *
     * @return Issue
     */
    public function addChild(Issue $child): Issue
    {
        if (!$this->getChildren()->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * Remove child.
     *
     * @param Issue $child
     *
     * @return Issue
     */
    public function removeChild(Issue $child): Issue
    {
        $this->children->removeElement($child);

        return $this;
    }

    /**
     * Get children.
     *
     * @return Collection|Issue[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * Add relatedWith.
     *
     * @param Issue $relatedWith
     *
     * @return Issue
     */
    public function addRelatedWith(Issue $relatedWith): Issue
    {
        if (!$this->getRelatedWith()->contains($relatedWith)) {
            $this->relatedWith[] = $relatedWith;
            $relatedWith->addRelatedIssue($this);
        }

        return $this;
    }

    /**
     * Remove relatedWith.
     *
     * @param Issue $relatedWith
     *
     * @return Issue
     */
    public function removeRelatedWith(Issue $relatedWith): Issue
    {
        $this->relatedWith->removeElement($relatedWith);

        return $this;
    }

    /**
     * Get relatedWith.
     *
     * @return Collection|Issue[]
     */
    public function getRelatedWith(): Collection
    {
        return $this->relatedWith;
    }

    /**
     * @return array
     */
    public function getChildCodes(): array
    {
        $result = [];
        if ($this->getChildren()) {
            foreach ($this->getChildren() as $child) {
                $result[] = $child->getCode();
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return IssueTypeProvider::TYPES;
    }
}

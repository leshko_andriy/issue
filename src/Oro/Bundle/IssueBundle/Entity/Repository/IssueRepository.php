<?php

namespace Oro\Bundle\IssueBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Oro\Bundle\IssueBundle\Entity\Issue;
use Oro\Bundle\IssueBundle\Provider\IssueTypeProvider;

/**
 * Repository for Entity Issue
 */
class IssueRepository extends EntityRepository
{
    /**
     * get count statuses and their names
     * @return array
     */
    public function getCountStatusesWithNames(): array
    {
        return $this->createQueryBuilder('i')
            ->select(['status.name as statusName', 'COUNT(status.id) as value'])
            ->join('i.status', 'status')
            ->groupBy('status.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Issue|null $issue
     * @return QueryBuilder
     */
    public function getIssuesExcludeMe(?Issue $issue): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('i')
            ->orderBy('i.id', 'DESC');
        if ($issue && $issue->getId()) {
            $queryBuilder->where('i.id <> :id')
                ->setParameter('id', $issue->getId());
        }

        return $queryBuilder;
    }

    /**
     * @param Issue|null $issue
     * @return QueryBuilder
     */
    public function getStoryIssuesExcludeMe(?Issue $issue): QueryBuilder
    {
        $params['storyType'] = IssueTypeProvider::STORY_TYPE;
        $queryBuilder = $this->createQueryBuilder('i')
            ->orderBy('i.id', 'DESC')
            ->where('i.type = :storyType');
        if ($issue && $issue->getId()) {
            $params['id'] = $issue->getId();
            $queryBuilder->andWhere('i.id <> :id');
        }
        $queryBuilder->setParameters($params);

        return $queryBuilder;
    }
}

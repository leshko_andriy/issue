define(function(require) {
    'use strict';

    var mediator = require('oroui/js/mediator');
    var BaseView = require('oroui/js/app/views/base/view');
    var widgetManager = require('oroui/js/widget-manager');
    var dateUpdated = BaseView.extend({

        /**
         * @inheritDoc
         */
        constructor: function dateUpdated() {
            dateUpdated.__super__.constructor.apply(this, arguments);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            var widgetName = options.widgetName;
            mediator.on('widget_success:note-dialog', function() {
                widgetManager.getWidgetInstanceByAlias(widgetName, function(widget) {
                    widget.loadContent();
                });
            }, this);
        }

    });

    return dateUpdated;
});

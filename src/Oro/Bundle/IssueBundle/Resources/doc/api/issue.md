# Extend\Entity\EV_Issue_Status

## ACTIONS

### get

Retrieve a specific issue status record.

Issue status is the state the issue is in (Open, In Progress, Resolved, Reopen, Closed).

### get_list

Retrieve a collection of issue statuses.

Issue status is the state the issue is in (Open, In Progress, Resolved, Reopen, Closed).

# Extend\Entity\EV_Issue_Priority

## ACTIONS

### get

Retrieve a specific issue priority record.

Issue priority is the state the issue is in (Major, Blocker, Critical, Minor, Trivial).

### get_list

Retrieve a collection of issue priorities.

Issue priority is the state the issue is in (Major, Blocker, Critical, Minor, Trivial).

# Extend\Entity\EV_Issue_Resolution

## ACTIONS

### get

Retrieve a specific issue resolution record.

Issue resolution is the state the issue is in (Done, Fixed, Duplicate, Incomplete, Will not fix, Failed, Declined).

### get_list

Retrieve a collection of issue resolution.

Issue resolution is the state the issue is in (Done, Fixed, Duplicate, Incomplete, Will not fix, Failed, Declined).

# Oro\Bundle\IssueBundle\Entity\Issue

## ACTIONS

### get_list

Retrieve a collection of issues.

{@inheritdoc}

### delete

Delete a specific issue record.

{@inheritdoc}

### get

Retrieve a specific issue record.

{@inheritdoc}

### create

Create a new issue record.

The created issue is returned in the response.

{@inheritdoc}

{@request:json_api}
Example:

```JSON
{  
   "data":{  
      "type":"issues",
      "attributes":{  
         "issueType": "SubTask",
         "summary":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit",
         "description":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
      },
      "relationships":{  
         "status":{  
            "data":{  
               "type":"issuestatuses",
               "id":"open"
            }
         },
         "priority":{  
            "data":{  
               "type":"issuepriorities",
               "id":"major"
            }
         },
         "assignee":{  
            "data":{  
               "type":"users",
               "id":"1"
            }
         },
         "parent":{  
            "data":{  
               "type":"issues",
               "id":"1"
            }
         }, 
         "organization":{  
            "data":{  
               "type":"organizations",
               "id":"1"
            }
         },
         "relatedIssues":{  
            "data":[  
                {  
                    "type":"issues",
                    "id":"2"
                },
                {  
                    "type":"issues",
                    "id":"3"
                }
            ]
         } 
      }
   }
}
```
{@/request}

### update

Edit a specific issue record.

{@inheritdoc}

{@request:json_api}
Example:

```JSON
{  
   "data":{  
      "type":"issues",
      "id": "1",
      "attributes":{  
         "issueType": "SubTask",
         "summary":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit",
         "description":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
      },
      "relationships":{  
         "status":{  
            "data":{  
               "type":"issuestatuses",
               "id":"open"
            }
         },
         "priority":{  
            "data":{  
               "type":"issuepriorities",
               "id":"major"
            }
         },
         "resolution":{  
            "data":{  
               "type":"issueresolutions",
               "id":"done"
            }
         },
         
         "assignee":{  
            "data":{  
               "type":"users",
               "id":"1"
            }
         },
         "parent":{  
            "data":{  
               "type":"issues",
               "id":"1"
            }
         }, 
         "organization":{  
            "data":{  
               "type":"organizations",
               "id":"1"
            }
         },
         "relatedIssues":{  
            "data":[  
                {  
                    "type":"issues",
                    "id":"2"
                },
                {  
                    "type":"issues",
                    "id":"3"
                }
            ]
         } 
      }
   }
}
```
{@/request}

### delete_list

Delete a issue records.

{@inheritdoc}

## FIELDS

### id

#### update

{@inheritdoc}

**The required field**

### summary

#### update

{@inheritdoc}

**The required field**

#### create

{@inheritdoc}

**The required field**

### description

#### update

{@inheritdoc}

**The required field**

#### create

{@inheritdoc}

**The required field**

### type

#### update

{@inheritdoc}

**The required field**

#### create

{@inheritdoc}

**The required field**

### priority

#### update

{@inheritdoc}

**The required field**

#### create

{@inheritdoc}

**The required field**

### assignee

#### update

{@inheritdoc}

**The required field**

#### create

{@inheritdoc}

**The required field**

## SUBRESOURCES

### assignee

#### get_subresource

Retrieve the record of the user who is assignee to issue record.

#### get_relationship

Retrieve the ID of the user who is assignee to issue record.

#### update_relationship

Replace the assignee of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "users",
    "id": "37"
  }
}
```
{@/request}

### parent

#### get_subresource

Retrieve the record of the issue what is parent to current issue record.

#### get_relationship

Retrieve the ID of the the issue what is parent to current issue record.

#### update_relationship

Replace the parent issue of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "issues",
    "id": "37"
  }
}
```
{@/request}

### status

#### get_subresource

Retrieve the record of the status of current issue record.

#### get_relationship

Retrieve the ID of the the status of current issue record.

#### update_relationship

Replace the status issue of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "issuestatuses",
    "id": "open"
  }
}
```
{@/request}

### priority

#### get_subresource

Retrieve the record of the priority of current issue record.

#### get_relationship

Retrieve the ID of the the priority of current issue record.

#### update_relationship

Replace the priority issue of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "issuepriorities",
    "id": "major"
  }
}
```
{@/request}

### resolution

#### get_subresource

Retrieve the record of the resolution of current issue record.

#### get_relationship

Retrieve the ID of the the resolution of current issue record.

#### update_relationship

Replace the resolution issue of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "issueresolutions",
    "id": "major"
  }
}
```
{@/request}

### reporter

#### get_subresource

Retrieve the record of the user who is reporter to issue record.

#### get_relationship

Retrieve the ID of the user who is reporter to issue record.

### organization

#### get_subresource

Retrieve the record of the organization what is owner to issue record.

#### get_relationship

Retrieve the ID of the organization what is owner to issue record.

#### update_relationship

Replace the organization of a specific issue record.

{@request:json_api}
Example:

```JSON
{
  "data": {
    "type": "organizations",
    "id": "7"
  }
}
```
{@/request}

### children

#### get_subresource

Retrieve the records of the issues what are children records to current issue record.

#### get_relationship

Retrieve the IDs of the issues what are children records to current issue record.

### collaborators

#### get_subresource

Retrieve the records of the users what are collaborators to current issue record.

#### get_relationship

Retrieve the IDs of the users what are collaborators to current issue record.

### relatedIssues

#### get_subresource

Retrieve the records of the issues what are related to current issue record.

#### get_relationship

Retrieve the IDs of the issues what are related to current issue record.

#### add_relationship

Add related issue to current issue.

{@request:json_api}
Example:

```JSON
{
  "data": [
    {
      "type": "issues",
      "id": "2"
    }
  ]
}
```
{@/request}

#### delete_relationship

Delete related issue to current issue.

{@request:json_api}
Example:

```JSON
{
  "data": [
    {
      "type": "issues",
      "id": "2"
    }
  ]
}
```
{@/request}

### relatedWith

#### get_subresource

Retrieve the records of the issues what are related with current issue record.

#### get_relationship

Retrieve the IDs of the issues what are related with current issue record.










